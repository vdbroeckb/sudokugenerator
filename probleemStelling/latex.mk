# Tools
LATEXMK = latexmk
RM = rm -f

# Project-specific settings
DOCNAME = main

# Targets
all: doc
doc: pdf
pdf: $(DOCNAME).pdf

# Rules
%.pdf: %.tex
        $(LATEXMK) -pdf -M -MP -MF $*.d $*

mostlyclean:
        $(LATEXMK) -silent -c -pdf
        $(RM) *.bbl

clean: mostlyclean
        $(LATEXMK) -silent -C -pdf
        $(RM) *.run.xml *.synctex.gz
        $(RM) *.d

.PHONY: all clean doc mostlyclean pdf

# Include auto-generated dependencies
-include *.d
