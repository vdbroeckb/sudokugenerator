:- use_module(library(clpfd)).
sudoku(Rows) :-
        length(Rows, 9), maplist(same_length(Rows), Rows),
        append(Rows, Vs), Vs ins 1..9,
        maplist(all_distinct, Rows),
        transpose(Rows, Columns),
        maplist(all_distinct, Columns),
        Rows = [As,Bs,Cs,Ds,Es,Fs,Gs,Hs,Is],
        blocks(As, Bs, Cs),
        blocks(Ds, Es, Fs),
        blocks(Gs, Hs, Is).

blocks([], [], []).
blocks([N1,N2,N3|Ns1], [N4,N5,N6|Ns2], [N7,N8,N9|Ns3]) :-
        all_distinct([N1,N2,N3,N4,N5,N6,N7,N8,N9]),
        blocks(Ns1, Ns2, Ns3).



problem(1,[[0,0,2,0,0,0,5,0,0],
   		   [0,1,0,7,0,5,0,2,0],
           [4,0,0,0,9,0,0,0,7],
           [0,4,9,0,0,0,7,3,0],
           [8,0,1,0,3,0,4,0,9],
           [0,3,6,0,0,0,2,1,0],
           [2,0,0,0,8,0,0,0,4],
           [0,8,0,9,0,2,0,6,0],
           [0,0,7,0,0,0,8,0,0]]).

problem(2,[[[3, 6, 7, 9],[6, 7, 9],2,[1, 3, 4, 6, 8],[1, 4, 6],[1, 3, 4, 6, 8],5,[4, 8, 9],[1, 3, 6, 8]],
           [[3, 6, 9],1,[3, 8],7,[4, 6],5,[3, 6, 9],2,[3, 6, 8]],
           [4,[5, 6],[3, 5, 8],[1, 2, 3, 6, 8],9,[1, 3, 6, 8],[1, 3, 6],[8],7],
           [[5],4,9,[1, 2, 5, 6, 8],[1, 2, 5, 6],[1, 6, 8],7,3,[5, 6, 8]],
           [8,[2, 5, 7],1,[2, 5, 6],3,[6, 7],4,[5],9],
           [[5, 7],3,6,[4, 5, 8],[4, 5, 7],[4, 7, 8, 9],2,1,[5, 8]],
           [2,[5, 6, 9],[3, 5],[1, 3, 5, 6],8,[1, 3, 6, 7],[1, 3, 9],[5, 7, 9],4],
           [[1, 3, 5],8,[3, 4, 5],9,[1, 4, 5, 7],2,[1, 3],6,[1, 3, 5]],
           [[1, 3, 5, 6, 9],[5, 6, 9],7,[1, 3, 4, 5, 6],[1, 4, 5, 6],[1, 3, 4, 6],8,[5, 9],[1, 2, 3, 5]]]).

problem(3,[[2,[5, 8],6,[1, 8],[1, 3, 5],[1, 3, 5, 8],[1, 3, 5, 7],4,9],[[5, 8],3,7,[1, 2, 4, 6, 8],[1, 2, 4, 5],9,[1, 5],[1, 2, 5, 8],[2, 5]],[1,[4, 5, 8, 9],[4, 8],7,[2, 3, 4, 5],[3, 5, 8],[3, 5],[2, 3, 5, 8],6],[[3, 6],[2, 4, 6],[1, 2, 3, 4],5,8,[1, 3, 7],9,[1, 2, 3, 6, 7],[2, 3, 7]],[7,[2, 6],5,[1, 9],[1, 3, 9],[1, 3],8,[1, 2, 3, 6],4],[[3, 8],[4, 8],9,[1, 4],6,2,[1, 3, 5, 7],[1, 3, 5, 7],[3, 5, 7]],[9,[2, 5, 6, 7, 8],[2, 3, 8],[2, 6, 8],[2, 5, 7],4,[3, 5, 6, 7],[3, 5, 6, 7],1],[[5, 6, 8],[2, 5, 6, 7, 8],[2, 8],3,[1, 2, 5, 7],[1, 5, 6, 7, 8],4,9,[5, 7]],[4,1,[3],[6, 9],[5, 7, 9],[5, 6, 7],2,[3, 5, 6, 7],8]]).

problem(4,[[[8],4,[3, 5],[2, 3, 6],[2, 3],[2, 3, 6],1,7,9],[[1, 7],[3, 9],2,[3, 6, 7, 9],[1, 3],8,[3, 6],5,4],[[1, 7],[3, 9],6,[2, 3, 7, 9],[1, 2, 3, 4],5,[3],[2],8],[[2, 4, 6],8,[3, 4],[2, 3, 5],7,[2, 3, 4],9,1,[2, 5, 6]],[[2, 4, 6, 7],5,[4, 7],[2, 8],9,[1, 2, 4],[6, 8],3,[2, 6, 7]],[[2, 7],1,9,[2, 3, 5, 8],6,[2, 3],[5, 8],4,[2, 5, 7]],[3,[6],[1],4,[2, 5, 8],[2, 6, 9],7,[8, 9],[1, 5]],[5,7,[4],1,[3, 8],[3, 6, 9],2,[8, 9],[3]],[9,2,8,[3, 5, 7],[3, 5],[3, 7],[3, 4, 5],6,[1, 3, 5]]]).
problem(5,[ [6,7,[5, 9],[2, 9],[2, 4, 5, 9],8,2,1,3],
            [[1, 3, 5, 8, 9],2,[5, 8, 9],[1, 7, 9],6,[4, 5, 7, 9],[4, 5, 9],[3, 4, 5, 7, 8, 9],[3, 4, 5, 7, 8]],
            [[1, 5, 8, 9],4,[5, 8, 9],[1, 2, 7, 9],3,[2, 4, 5, 7, 9],6,[2, 4, 5, 6, 7, 8, 9],[4, 5, 7, 8]],
            [2,[3, 5, 9],1,[3, 7, 8, 9],[4, 5, 7, 8, 9],[3, 4, 5, 7, 9],[4, 5],[3, 4, 5, 8],6],
            [4,8,[5, 6, 9],[2, 3, 6, 9],[2, 5, 9],1,7,[2, 3, 5],[3, 5]],
            [[3, 5, 7],[3, 5, 6],[5, 6, 7],[2, 3, 6, 7, 8],[2, 4, 5, 7, 8],[2, 3, 4, 5, 6, 7],1,[2, 3, 4, 5, 8],9],
            [[1, 7, 8, 9],[1, 6, 9],4,5,[1, 2, 7, 8, 9],3,1,[6, 7, 9],[1, 7]],
            [[1, 5, 7, 8, 9],[1, 5, 6, 9],2,[1, 2, 6, 7, 8, 9],[1, 2, 7, 8, 9],[2, 6, 7, 9],3,[4, 5, 6, 7, 9],[1, 4, 5, 7]],
            [[1, 5, 7, 9],[1, 5, 6, 9],3,4,[1, 7, 9],[6, 7, 9],8,[5, 6, 7, 9],2]]).

problem(6,[ [6,7,[5, 9],[2, 9],[4, 5, 9],8,[2, 4, 5, 9],1,3],
            [3,2,[5, 8, 9],1,6,[4, 5, 7, 9],[4, 5, 9],[4, 5, 7, 8, 9],[4, 5, 7, 8]],
            [1,4,[5, 8, 9],[2, 9],3,[2, 5, 7, 9],[2, 5, 6, 9],[2, 5, 6, 7, 8, 9],[5, 7, 8]],
            [2,[3, 5, 9],1,[3, 7, 8, 9],[4, 5, 7, 8, 9],[4, 5, 7, 9],[4, 5],[3, 4, 5, 8],6],
            [4,8,6,[2, 3, 9],[5, 9],1,7,[2, 3, 5],[5]],
            [[5],[3, 5],7,[2, 3, 6, 8],[4, 5, 8],[2, 4, 5, 6],1,[2, 3, 4, 5, 8],9],
            [8,[1, 6, 9],4,5,2,3,[6, 9],[6, 7, 9],[1, 7]],
            [[5, 7, 9],[1, 5, 6, 9],2,[6, 7, 8, 9],[1, 7, 8, 9],[6, 7, 9],3,[4, 5, 6, 7, 9],[1, 4, 5, 7]],
            [[5, 7, 9],[1, 5, 6, 9],3,4,[1, 7, 9],[6, 7, 9],8,[5, 6, 7, 9],2]]).

problem(7,[[[2, 5, 6],1,4,[5, 6, 7, 8],[3, 5, 6, 7, 8],[3, 5, 6, 7, 8],9,[3, 5],[2, 3, 5, 6]],
            [[2, 5, 6],3,[2, 5, 6],4,9,1,[6, 8],7,[2, 5, 6, 8]],
            [7,8,9,2,[3, 5, 6],[3, 5, 6],[3, 6],4,1],
            [[3, 5, 9],4,1,[5, 6, 7, 8],[3, 5, 6, 7, 8],[3, 5, 6, 7, 8, 9],2,[3, 5, 8],[3, 5, 8]],
            [8,6,[2, 3, 5],1,[2, 3, 5],[3, 5],4,9,7],
            [[2, 3, 5, 9],[2, 5, 9],7,[5, 8],[2, 3, 4, 5, 8],[3, 4, 5, 8, 9],1,6,[3, 5, 8]],
            [1,7,[3, 5, 6],9,[5, 6, 8],2,[3, 6, 8],[3, 8],4],
            [[4, 5, 6, 9],[5, 9],[5, 6],3,1,[4, 5, 6, 8],7,2,[6, 8, 9]],
            [[2, 3, 4, 6, 9],[2, 9],8,[6, 7],[4, 6, 7],[4, 6, 7],5,1,[3, 6, 9]]]).

problem(8,[[8,9,1,5,7,6,2,3,4],[6,[3, 5],[3, 4, 5],1,[2, 3],[2, 4],9,7,8],[2,[3, 7],[3, 4, 7],9,[3, 8],[4, 8],5,1,6],[7,8,6,[2, 3, 4],[2, 3, 5],[2, 4, 5],[3, 4],9,1],[5,1,[2, 9],[2, 3, 4],6,[2, 4, 8, 9],[3, 4, 7, 8],[4, 8],[2, 3, 7]],[3,4,[2, 9],7,[2, 5, 8, 9],1,6,[5, 8],[2, 5]],[9,[2, 3, 5],8,[2, 6],[1, 2, 5],7,[1, 3, 4],[4, 5, 6],[3, 5]],[4,[2, 5, 7],[2, 5, 7],[2, 6],[1, 2, 5, 9],3,[1, 7, 8],[5, 6, 8],[5, 7, 9]],[1,6,[3, 5, 7],8,4,[5, 9],[3, 7],2,[3, 5, 7, 9]]]).

problem(9,[[6,4,5,7,[1, 2],9,[1, 2, 3],8,[1, 3]],[8,[1, 3],[1, 2, 3, 7],4,[1, 2],5,6,[1, 7],9],[[1, 2],9,[1, 2, 7],3,6,8,[1, 2, 4],5,[4, 7]],[3,7,9,[1, 5],8,2,[1, 4],6,[4, 5]],[4,8,[1, 2],[1, 5],7,6,9,3,[2, 5]],[[1, 2],5,6,9,[3, 4],[3, 4],8,[1, 7],[2, 7]],[7,2,8,6,[3, 4],[1, 3, 4],5,9,[1, 3]],[9,[1, 3],4,8,5,[1, 3],7,2,6],[5,6,[1, 3],2,9,7,[1, 3],4,8]]).

problem(10,[[6,4,5,7,[1, 2],9,[1, 2, 3],8,[1, 3]],[8,[1, 3],[1, 2, 3, 7],4,[1, 2],5,6,[1, 7],9],[[1, 2],9,[1, 2, 7],3,6,8,[1, 2, 4],5,[4, 7]],[3,7,9,[1, 5],8,2,[1, 4],6,[4, 5]],[4,8,[1, 2],[1, 5],7,6,9,3,[2, 5]],[[1, 2],5,6,9,[3, 4],[3, 4],8,[1, 7],[2, 7]],[7,2,8,6,[3, 4],[1, 3, 4],5,9,[1, 3]],[9,[1, 3],4,8,5,[1, 3],7,2,6],[5,6,[1, 3],2,9,7,[1, 3],4,8]]).


fill_variables([],[]).
fill_variables([XHead|XTail],[YHead|YTail]):-
	fill_col_variables(XHead,YHead),
    fill_variables(XTail,YTail).

fill_col_variables([],[]).
fill_col_variables([XHead|XTail],[XHead|YTail]):-
    XHead in 1..9,
    fill_col_variables(XTail,YTail).

fill_col_variables([XHead|XTail],[YHead|YTail]):-
    \+ XHead in 1..9,
    YHead in 1..9,
    fill_col_variables(XTail,YTail).

fill_pencils(_,[],[],_).
fill_pencils(Rows,[XHead|XTail],[YHead|YTail],Col):-
    Coln is Col +1,
	fill_col_pencils(Rows,XHead,YHead, Col,0),
    fill_pencils(Rows,XTail,YTail, Coln), !.

fill_col_pencils(_,[],[],_,_).
fill_col_pencils(Rows,[XHead|XTail],[XHead|YTail],Col,Row):-
    XHead in 1..9,
    Rown is Row + 1,
    fill_col_pencils(Rows,XTail,YTail,Col,Rown).

fill_col_pencils(Rows,[XHead|XTail],[YHead|YTail],Col,Row):-
    \+ XHead in 1..9,
	calc_pencils(Rows, coor(Col,Row),YHead),
	%YHead = 'a',
    Rown is Row + 1,
    fill_col_pencils(Rows,XTail,YTail,Col,Rown).

calc_pencils(Rows, coor(Col,Row),Pencils):-
    neighbours(Rows, coor(Col,Row),CRB),
    difference([1,2,3,4,5,6,7,8,9],CRB,UPencils),
    sort(UPencils,Pencils).

neighbours(Rows, coor(Col,Row),CRB):-
    column(Rows,Col,C),
    row(Rows, Row, R),
    ColQ is Col div 3,
    RowQ is Row div 3,
    block_list(Rows, ColQ, RowQ, B),
    append(C,R,CR),
    append(CR,B,CRB).

lt(X,Y):-var(X);var(Y).
lt(X,Y):-nonvar(X),nonvar(Y),X<Y.

difference([],_Y,[]).
difference([X|TX],Y,Z):-
   member(X,Y),
   difference(TX,Y,Z).

difference([X|TX],Y,[X|Z]):-
   \+ member(X,Y),
   difference(TX,Y,Z).

union([],S,S).
union(S,[],S):-S\=[].
union([X|TX],[X|TY],[X|TZ]):-
   union(TX,TY,TZ).
union([X|TX],[Y|TY],[X|TZ]):-
   lt(X,Y),
   union(TX,[Y|TY],TZ).
union([X|TX],[Y|TY],[Y|TZ]):-
   lt(Y,X),
   union([X|TX],TY,TZ).

%problem(1, [[_,_,_,_,_,_,_,_,_],
%            [_,_,_,_,_,3,_,8,5],
%            [_,_,1,_,2,_,_,_,_],
%            [_,_,_,5,_,7,_,_,_],
%            [_,_,4,_,_,_,1,_,_],
%            [_,9,_,_,_,_,_,_,_],
%            [5,_,_,_,_,_,_,7,3],
%            [_,_,2,_,1,_,_,_,_],
%            [_,_,_,_,4,_,_,_,9]]).

row(Rows, N, Row):-
	transpose(Rows, Columns),
    nth0(N,Columns,Row).
column(Rows,N,Col):-
    nth0(N,Rows,Col).

block(Rows, 0, 0, Block):-
	Rows = [As,Bs,Cs|_Rest],
	As = [N1,N2,N3|_Ns1],
	Bs = [N4,N5,N6|_Ns2],
	Cs = [N7,N8,N9|_Ns3],
	Block = [[N1,N2,N3],[N4,N5,N6],[N7,N8,N9]].

block(Rows, Col, Row, Block):-
	Rows = [_As,_Bs,_Cs|Rest],
    ColN is Col - 1,
	block(Rest,ColN,Row,Block).

block(Rows, Col, Row, Block):-
    Rows = [As,Bs,Cs|_Rest],
	As = [_N1,_N2,_N3|Ns1],
	Bs = [_N4,_N5,_N6|Ns2],
	Cs = [_N7,_N8,_N9|Ns3],
    RowN is Row - 1,
	block([Ns1,Ns2,Ns3],Col,RowN,Block).

block_list(Rows, Col, Row, List):-
    block(Rows, Col, Row, Block),
    flat(Block,List).

flat([],[]).
flat([H|T],R) :- flat(T,T1), append(H,T1,R).


coor(X,Y):-
    X in 0..8,
    Y in 0..8.

in_col(coor(X,Y),X):-
    coor(X,Y).


all_col_coors(L):-
    all_col_coors(L,8).

all_col_coors([L],0):-
    col_coors(0,L).

all_col_coors([L|Tail],N):-
    NN is N -1,
    col_coors(N,L),
    all_col_coors(Tail,NN).

col_coors(Col, L):-
    member(Col,[0,1,2,3,4,5,6,7,8]),
    col_coors(Col, L, 0,8).

col_coors(Col,[coor(Col, Row)], Row, 0):- !.
col_coors(Col,[coor(Col, Row)|Tail], Row, N):-
    NN is N -1,
    RowN is Row +1,
    col_coors(Col,Tail, RowN, NN).


in_row(coor(_,Y),Y).

all_row_coors(L):-
    all_row_coors(L,8).

all_row_coors([L],0):-
    row_coors(0,L).

all_row_coors([L|Tail],N):-
    NN is N -1,
    row_coors(N,L),
    all_row_coors(Tail,NN).

row_coors(Row, L):-
    member(Row,[0,1,2,3,4,5,6,7,8]),
    row_coors(Row, L, 0,8).

row_coors(Row,[coor(Col, Row)], Col, 0):- !.
row_coors(Row,[coor(Col, Row)|Tail], Col, N):-
    NN is N -1,
    ColN is Col +1,
    row_coors(Row,Tail, ColN, NN).

in_block(coor(X,Y),Col,Row):-
    Xmin is (Col*3), Xmax is ((Col+1)*3) - 1,
    Ymin is (Row*3), Ymax is ((Row+1)*3) - 1,
    X in Xmin..Xmax,
    Y in Ymin..Ymax.

block_coors(X, Y, L):-
    member(X,[0,3,6]),
    member(Y,[0,3,6]),
	L = [C1, C2, C3, C4, C5, C6, C7, C8, C9],
	Y1 is Y +1, Y2 is Y + 2,
	col_coors(Y, [C1, C2, C3], X, 2),
	col_coors(Y1, [C4, C5, C6], X, 2),
	col_coors(Y2, [C7, C8, C9], X, 2).

cell(Rows, coor(Col, Row), Cell):-
    nth0(Col,Rows,C),
    nth0(Row,C,Cell).

cells(_, [], []).
cells(Rows, [coor(Col,Row)|Tail], [Cell|CTail]):-
    cell(Rows,  coor(Col,Row), Cell),
    cells(Rows, Tail, CTail).

%
%===Single Position===
%

single_position_list(Rows, List):-
    findall([X,Y,Z],single_position(Rows,coor(X,Y),Z), List).

single_position(Rows, coor(Col,Row), Value):-
    cell(Rows,coor(Col,Row),Cell),
    member(Value,Cell),
    only_pencil_row(Rows, Row, Value).

single_position(Rows, coor(Col,Row), Value):-
    cell(Rows,coor(Col,Row),Cell),
    member(Value,Cell),
    only_pencil_col(Rows, Col, Value).

single_position(Rows, coor(Col,Row), Value):-
    cell(Rows,coor(Col,Row),Cell),
    member(Value,Cell),
    ColQ is Col div 3,
    RowQ is Row div 3,
    only_pencil_block(Rows, ColQ, RowQ, Value).

only_pencil_row(Rows, Row, Value):-
    row(Rows, Row, R),
    count_pencils(R, Value,0, 1).

only_pencil_col(Rows, Col, Value):-
    column(Rows, Col, C),
    count_pencils(C, Value,0, 1).

only_pencil_block(Rows, Col, Row, Value):-
    block_list(Rows, Col, Row, List),
    count_pencils(List, Value,0, 1).

count_pencils([],_,X,X).
count_pencils([Head|Tail],Value,N,X):-
    member(Value,Head),
    NN is N + 1,
    count_pencils(Tail,Value, NN, X).

count_pencils([Head|Tail],Value,N,X):-
    \+member(Value,Head),
    count_pencils(Tail,Value, N,X).

%
%===Single Candidate ===
%

single_candidate(Rows, coor(X,Y),Value):-
    cell(Rows,coor(X,Y),[Value]).

single_candidate_list(Rows, List):-
    findall([X,Y,Z],single_candidate(Rows,coor(X,Y),Z), List).


%
%===Candidate Lines===
%

candidate_lines_list(Rows, List):-
    candidate_lines_list(Rows, List, 9).

candidate_lines_list(_, [], 0):-!.

candidate_lines_list(Rows, List, Value):-
    N is Value - 1,
    candidate_lines_list(Rows, L, N),
    findall([Coors,Value], candidate_lines(Rows, Coors, Value), All),
    append(All, L, List).

candidate_lines(Rows, Coors, Value):-
    member(Value,[1,2,3,4,5,6,7,8,9]),
    row_coors(_, Row),
    block_coors(_,_,B),
    where(Rows, B, Value, L),
    sublist(L, Row),
    \+ L = [],
    substract(Row, L, List),
    where(Rows, List, Value, Coors),
    \+ Coors = [].

candidate_lines(Rows, Coors, Value):-
    member(Value,[1,2,3,4,5,6,7,8,9]),
    col_coors(_, Row),
    block_coors(_,_,B),
    where(Rows, B, Value, L),
    sublist(L, Row),
    \+ L = [],
    substract(Row, L, List),
    where(Rows, List, Value, Coors),
    \+ Coors = [].

where(Rows, Region, Value, Where):-
    Value in 1..9,
    findallPencils(Rows, Value, List),
    intersect(Region, List, Where).

findallPencils(Rows, Value, List):-
    findall(coor(X,Y), ( coor(X,Y),cell(Rows, coor(X,Y), Pencils), member(Value,Pencils) ) , List).

intersect([], _, []).

intersect([H1|T1], L2, [H1|Res]) :-
    member(H1, L2),
    intersect(T1, L2, Res).

intersect([H1|T1], L2, Res) :-
    \+member(H1, L2),
    intersect(T1, L2, Res).

substract([], _, []).

substract([H1|T1], L2, [H1|Res]) :-
    \+member(H1, L2),
    substract(T1, L2, Res).

substract([H1|T1], L2, Res) :-
    member(H1, L2),
    substract(T1, L2, Res).

sublist([], _).

sublist([H1|T1], L2) :-
    member(H1, L2),
    sublist(T1, L2).

%
%===Multiple Lines===
%

multiple_lines_list(Rows, List):-
    multiple_lines_list(Rows, List, 9).
multiple_lines_list(_, [], 0):-!.

multiple_lines_list(Rows, List, Value):-
    N is Value - 1,
    multiple_lines_list(Rows, L, N),
    findall([Coors,Value], multiple_lines(Rows, Coors, Value), All),
    append(All, L, List).

multiple_lines(Rows, Coors, Value):-
	member(Value,[1,2,3,4,5,6,7,8,9]),
    all_row_coors(AR),
    member(Row, AR),
    block_coors(_,_,B),
    where(Rows, Row, Value, L),
    sublist(L, B),
    \+ L = [],
    substract(B, L, List),
    where(Rows, List, Value, Coors),
    \+ Coors = [].

multiple_lines(Rows, Coors, Value):-
	member(Value,[1,2,3,4,5,6,7,8,9]),
    all_col_coors(AC),
    member(Col, AC),
    block_coors(_,_,B),
    where(Rows, Col, Value, L),
    sublist(L, B),
    \+ L = [],
    substract(B, L, List),
    where(Rows, List, Value, Coors),
    \+ Coors = [].

%
%===Naked Pairs===
%

naked_pair_list(Rows, L):-
    findall(Coors,naked_pair(Rows, Coors, _,_), List),
    flat(List,FlatList),
    rm_empty_coors(FlatList,L).

naked_pair(Rows, Coors, Value1, Value2):-
    row_coors(_, Row),
    member(Value1,[1,2,3,4,5,6,7,8]),
    member(Value2,[2,3,4,5,6,7,8,9]),
    Value1 < Value2,
    member(Coor1, Row),
    member(Coor2,Row),
    Coor1 = coor(X1,Y1),
    Coor2 = coor(X2,Y2),
    Sum1 #= X1 + 9 * Y1,
    Sum2 #= X2 + 9 * Y2,
    Sum1 #< Sum2,
    cell(Rows, Coor1, [Value1,Value2]),
    cell(Rows, Coor2, [Value1,Value2]),
    substract(Row, [Coor1,Coor2], List),
    where(Rows, List, Value1, Coors1),
    where(Rows, List, Value2, Coors2),
    not_both_empty(Coors1,Coors2),
    Coors = [[Coors1, Value1],[Coors2,Value2]].

naked_pair(Rows, Coors, Value1, Value2):-
    col_coors(_, Row),
    member(Value1,[1,2,3,4,5,6,7,8]),
    member(Value2,[2,3,4,5,6,7,8,9]),
    Value1 < Value2,
    member(Coor1, Row),
    member(Coor2,Row),
    Coor1 = coor(X1,Y1),
    Coor2 = coor(X2,Y2),
    Sum1 #= X1 + 9 * Y1,
    Sum2 #= X2 + 9 * Y2,
    Sum1 #< Sum2,
    cell(Rows, Coor1, [Value1,Value2]),
    cell(Rows, Coor2, [Value1,Value2]),
    substract(Row, [Coor1,Coor2], List),
    where(Rows, List, Value1, Coors1),
    where(Rows, List, Value2, Coors2),
    not_both_empty(Coors1,Coors2),
    Coors = [[Coors1, Value1],[Coors2,Value2]].

naked_pair(Rows, Coors, Value1, Value2):-
    block_coors(_,_, Row),
    member(Value1,[1,2,3,4,5,6,7,8]),
    member(Value2,[2,3,4,5,6,7,8,9]),
    Value1 < Value2,
    member(Coor1, Row),
    member(Coor2,Row),
    Coor1 = coor(X1,Y1),
    Coor2 = coor(X2,Y2),
    Sum1 #= X1 + 9 * Y1,
    Sum2 #= X2 + 9 * Y2,
    Sum1 #< Sum2,
    cell(Rows, Coor1, [Value1,Value2]),
    cell(Rows, Coor2, [Value1,Value2]),
    substract(Row, [Coor1,Coor2], List),
    where(Rows, List, Value1, Coors1),
    where(Rows, List, Value2, Coors2),
    not_both_empty(Coors1,Coors2),
    Coors = [[Coors1, Value1],[Coors2,Value2]].

not_both_empty([],[_|_]).
not_both_empty([_|_],[]).
not_both_empty([_|_],[_|_]).

rm_empty_coors([],[]).
rm_empty_coors([[[],_]|Rest],L):-
    rm_empty_coors(Rest,L).
rm_empty_coors([[[X|XHead],XValue]|XRest],[[[X|XHead],XValue]|LRest]):-
    rm_empty_coors(XRest,LRest).

%
%===Naked Triple===
%

naked_triple_list(Rows, L):-
    findall(Coors,naked_triple(Rows, Coors, _,_), List),
    flat(List,FlatList),
    rm_empty_coors(FlatList,L).

naked_triple(Rows, Coors, Value1, Value2):-
    row_coors(_, Row),
    count_emptys(Rows, Row, Region),
	length(Region, Count),
    Count > 3 ,
    member(Value1,[1,2,3,4,5,6,7]),
    member(Value2,[2,3,4,5,6,7,8]),
    member(Value3,[3,4,5,6,7,8,9]),
    Value1 < Value2,
    Value2 < Value3,
    member(Coor1, Region),
    member(Coor2, Region),
    member(Coor3, Region),
    Coor1 = coor(X1,Y1),
    Coor2 = coor(X2,Y2),
    Coor3 = coor(X3,Y3),
    Sum1 #= X1 + 9 * Y1,
    Sum2 #= X2 + 9 * Y2,
    Sum3 #= X3 + 9 * Y3,
    Sum1 #< Sum2,
    Sum2 #< Sum3,
    cell(Rows, Coor1, Pencils1),
    substract(Pencils1, [Value1,Value2,Value3],[]),
    cell(Rows, Coor2, Pencils2),
    substract(Pencils2, [Value1,Value2,Value3],[]),
    cell(Rows, Coor3, Pencils3),
    substract(Pencils3, [Value1,Value2,Value3],[]),
    substract(Region, [Coor1,Coor2,Coor3], List),
    where(Rows, List, Value1, Coors1),
    where(Rows, List, Value2, Coors2),
    where(Rows, List, Value3, Coors3),
    not_all_empty(Coors1,Coor2,Coors3),
    Coors = [[Coors1, Value1],[Coors2,Value2],[Coors3,Value3]].

naked_triple(Rows, Coors, Value1, Value2):-
    col_coors(_, Col),
    count_emptys(Rows, Col, Region),
	length(Region, Count),
    Count > 3 ,
    member(Value1,[1,2,3,4,5,6,7]),
    member(Value2,[2,3,4,5,6,7,8]),
    member(Value3,[3,4,5,6,7,8,9]),
    Value1 < Value2,
    Value2 < Value3,
    member(Coor1, Region),
    member(Coor2, Region),
    member(Coor3, Region),
    Coor1 = coor(X1,Y1),
    Coor2 = coor(X2,Y2),
    Coor3 = coor(X3,Y3),
    Sum1 #= X1 + 9 * Y1,
    Sum2 #= X2 + 9 * Y2,
    Sum3 #= X3 + 9 * Y3,
    Sum1 #< Sum2,
    Sum2 #< Sum3,
    cell(Rows, Coor1, Pencils1),
    substract(Pencils1, [Value1,Value2,Value3],[]),
    cell(Rows, Coor2, Pencils2),
    substract(Pencils2, [Value1,Value2,Value3],[]),
    cell(Rows, Coor3, Pencils3),
    substract(Pencils3, [Value1,Value2,Value3],[]),
    substract(Region, [Coor1,Coor2,Coor3], List),
    where(Rows, List, Value1, Coors1),
    where(Rows, List, Value2, Coors2),
    where(Rows, List, Value3, Coors3),
    not_all_empty(Coors1,Coors2,Coors3),
    Coors = [[Coors1, Value1],[Coors2,Value2],[Coors3,Value3]].

naked_triple(Rows, Coors, Value1, Value2):-
    block_coors(_,_, Block),
    count_emptys(Rows, Block, Region),
	length(Region, Count),
    Count > 3 ,
    member(Value1,[1,2,3,4,5,6,7]),
    member(Value2,[2,3,4,5,6,7,8]),
    member(Value3,[3,4,5,6,7,8,9]),
    Value1 < Value2,
    Value2 < Value3,
    member(Coor1, Region),
    member(Coor2, Region),
    member(Coor3, Region),
    Coor1 = coor(X1,Y1),
    Coor2 = coor(X2,Y2),
    Coor3 = coor(X3,Y3),
    Sum1 #= X1 + 9 * Y1,
    Sum2 #= X2 + 9 * Y2,
    Sum3 #= X3 + 9 * Y3,
    Sum1 #< Sum2,
    Sum2 #< Sum3,
    cell(Rows, Coor1, Pencils1),
    substract(Pencils1, [Value1,Value2,Value3],[]),
    cell(Rows, Coor2, Pencils2),
    substract(Pencils2, [Value1,Value2,Value3],[]),
    cell(Rows, Coor3, Pencils3),
    substract(Pencils3, [Value1,Value2,Value3],[]),
    substract(Region, [Coor1,Coor2,Coor3], List),
    where(Rows, List, Value1, Coors1),
    where(Rows, List, Value2, Coors2),
    where(Rows, List, Value3, Coors3),
    not_all_empty(Coors1,Coors2,Coors3),
    Coors = [[Coors1, Value1],[Coors2,Value2],[Coors3,Value3]].

count_emptys(_, [], []).

count_emptys(Rows, [coor(X,Y)|Coors], Emptys):-
    cell(Rows, coor(X,Y), Cell),
    member(Cell,[1,2,3,4,5,6,7,8,9]),
    count_emptys(Rows, Coors, Emptys).

count_emptys(Rows, [coor(X,Y)|Coors], [coor(X,Y)|Emptys]):-
    cell(Rows, coor(X,Y), [_|_]),
    count_emptys(Rows, Coors, Emptys).

not_all_empty(_,_,[_|_]):-!.
not_all_empty(_,[_|_],_):-!.
not_all_empty([_|_],_,_):-!.

%
%===Hidden Pairs===
%

hidden_pair_list(Rows, L):-
    findall(Coors,hidden_pair(Rows, Coors, _,_), List),
    flat(List,FlatList),
    rm_empty_coors(FlatList,L).

hidden_pair(Rows, Coors, Value1, Value2):-
    row_coors(_, Region),
    member(Value1,[1,2,3,4,5,6,7,8]),
    member(Value2,[2,3,4,5,6,7,8,9]),
    Value1 < Value2,
    member(Coor1, Region),
    member(Coor2, Region),
    Coor1 = coor(X1,Y1),
    Coor2 = coor(X2,Y2),
    Sum1 #= X1 + 9 * Y1,
    Sum2 #= X2 + 9 * Y2,
    Sum1 #< Sum2,
    substract(Region, [Coor1,Coor2], List),
    where(Rows, List, Value1, []),
    where(Rows, List, Value2, []),
    cell(Rows, Coor1, Cell1),
    cell(Rows, Coor2, Cell2),
	intersect(Cell1, [Value1,Value2], [Value1,Value2]),
	intersect(Cell2, [Value1,Value2], [Value1,Value2]),
    substract(Cell1, [Value1,Value2], Values1),
    substract(Cell2, [Value1,Value2], Values2),
    findall([[Coor1],Value], member(Value,Values1),All1),
    findall([[Coor2],Value], member(Value,Values2),All2),
    append(All1,All2,Coors),
    \+Coors = [].

hidden_pair(Rows, Coors, Value1, Value2):-
    col_coors(_, Region),
    member(Value1,[1,2,3,4,5,6,7,8]),
    member(Value2,[2,3,4,5,6,7,8,9]),
    Value1 < Value2,
    member(Coor1, Region),
    member(Coor2, Region),
    Coor1 = coor(X1,Y1),
    Coor2 = coor(X2,Y2),
    Sum1 #= X1 + 9 * Y1,
    Sum2 #= X2 + 9 * Y2,
    Sum1 #< Sum2,
    substract(Region, [Coor1,Coor2], List),
    where(Rows, List, Value1, []),
    where(Rows, List, Value2, []),
    cell(Rows, Coor1, Cell1),
    cell(Rows, Coor2, Cell2),
	intersect(Cell1, [Value1,Value2], [Value1,Value2]),
	intersect(Cell2, [Value1,Value2], [Value1,Value2]),
    substract(Cell1, [Value1,Value2], Values1),
    substract(Cell2, [Value1,Value2], Values2),
    findall([[Coor1],Value], member(Value,Values1),All1),
    findall([[Coor2],Value], member(Value,Values2),All2),
    append(All1,All2,Coors),
    \+Coors = [].

hidden_pair(Rows, Coors, Value1, Value2):-
    block_coors(_,_, Region),
    member(Value1,[1,2,3,4,5,6,7,8]),
    member(Value2,[2,3,4,5,6,7,8,9]),
    Value1 < Value2,
    member(Coor1, Region),
    member(Coor2, Region),
    Coor1 = coor(X1,Y1),
    Coor2 = coor(X2,Y2),
    Sum1 #= X1 + 9 * Y1,
    Sum2 #= X2 + 9 * Y2,
    Sum1 #< Sum2,
    substract(Region, [Coor1,Coor2], List),
    where(Rows, List, Value1, []),
    where(Rows, List, Value2, []),
    cell(Rows, Coor1, Cell1),
    cell(Rows, Coor2, Cell2),
	intersect(Cell1, [Value1,Value2], [Value1,Value2]),
	intersect(Cell2, [Value1,Value2], [Value1,Value2]),
    substract(Cell1, [Value1,Value2], Values1),
    substract(Cell2, [Value1,Value2], Values2),
    findall([[Coor1],Value], member(Value,Values1),All1),
    findall([[Coor2],Value], member(Value,Values2),All2),
    append(All1,All2,Coors),
    \+Coors = [].

%
%===Hidden Triple===
%

hidden_triple_list(Rows, L):-
    findall(Coors,hidden_triple(Rows, Coors, _,_), List),
    flat(List,FlatList),
    rm_empty_coors(FlatList,L).

hidden_triple(Rows, Coors, Value1, Value2):-
    row_coors(_, Row),
    count_emptys(Rows, Row, Region),
	length(Region, Count),
    Count > 4 ,
    member(Value1,[1,2,3,4,5,6,7]),
    member(Value2,[2,3,4,5,6,7,8]),
    member(Value3,[3,4,5,6,7,8,9]),
    Value1 < Value2,
    Value2 < Value3,
    member(Coor1, Region),
    member(Coor2, Region),
    member(Coor3, Region),
    Coor1 = coor(X1,Y1),
    Coor2 = coor(X2,Y2),
    Coor3 = coor(X3,Y3),
    Sum1 #= X1 + 9 * Y1,
    Sum2 #= X2 + 9 * Y2,
    Sum3 #= X3 + 9 * Y3,
    Sum1 #< Sum2,
    Sum2 #< Sum3,
    substract(Region, [Coor1,Coor2,Coor3], List),
    where(Rows, List, Value1, []),
    where(Rows, List, Value2, []),
    where(Rows, List, Value3, []),
    cell(Rows, Coor1, Cell1),
    cell(Rows, Coor2, Cell2),
    cell(Rows, Coor3, Cell3),
	append(Cell1, Cell2, Cell12),
	append(Cell12, Cell3, Cell123),
	intersect([Value1,Value2,Value3], Cell123, [Value1,Value2,Value3]),
	intersect(Cell1, [Value1,Value2,Value3], [_|[_|_]]),
	intersect(Cell2, [Value1,Value2,Value3], [_|[_|_]]),
	intersect(Cell3, [Value1,Value2,Value3], [_|[_|_]]),
    substract(Cell1, [Value1,Value2,Value3], Values1),
    substract(Cell2, [Value1,Value2,Value3], Values2),
    substract(Cell3, [Value1,Value2,Value3], Values3),
    findall([[Coor1],Value], member(Value,Values1),All1),
    findall([[Coor2],Value], member(Value,Values2),All2),
    findall([[Coor3],Value], member(Value,Values3),All3),
    append(All1,All2,All12),
    append(All12,All3,Coors),
    \+Coors = [],!.

hidden_triple(Rows, Coors, Value1, Value2):-
    col_coors(_, Col),
    count_emptys(Rows, Col, Region),
	length(Region, Count),
    Count > 4 ,
    member(Value1,[1,2,3,4,5,6,7]),
    member(Value2,[2,3,4,5,6,7,8]),
    member(Value3,[3,4,5,6,7,8,9]),
    Value1 < Value2,
    Value2 < Value3,
    member(Coor1, Region),
    member(Coor2, Region),
    member(Coor3, Region),
    Coor1 = coor(X1,Y1),
    Coor2 = coor(X2,Y2),
    Coor3 = coor(X3,Y3),
    Sum1 #= X1 + 9 * Y1,
    Sum2 #= X2 + 9 * Y2,
    Sum3 #= X3 + 9 * Y3,
    Sum1 #< Sum2,
    Sum2 #< Sum3,
    substract(Region, [Coor1,Coor2,Coor3], List),
    where(Rows, List, Value1, []),
    where(Rows, List, Value2, []),
    where(Rows, List, Value3, []),
    cell(Rows, Coor1, Cell1),
    cell(Rows, Coor2, Cell2),
    cell(Rows, Coor3, Cell3),
	append(Cell1, Cell2, Cell12),
	append(Cell12, Cell3, Cell123),
	intersect([Value1,Value2,Value3], Cell123, [Value1,Value2,Value3]),
	intersect(Cell1, [Value1,Value2,Value3], [_|[_|_]]),
	intersect(Cell2, [Value1,Value2,Value3], [_|[_|_]]),
	intersect(Cell3, [Value1,Value2,Value3], [_|[_|_]]),
    substract(Cell1, [Value1,Value2,Value3], Values1),
    substract(Cell2, [Value1,Value2,Value3], Values2),
    substract(Cell3, [Value1,Value2,Value3], Values3),
    findall([[Coor1],Value], member(Value,Values1),All1),
    findall([[Coor2],Value], member(Value,Values2),All2),
    findall([[Coor3],Value], member(Value,Values3),All3),
    append(All1,All2,All12),
    append(All12,All3,Coors),
    \+Coors = [],!.

hidden_triple(Rows, Coors, Value1, Value2):-
    block_coors(_,_, Block),
    count_emptys(Rows, Block, Region),
	length(Region, Count),
    Count > 4 ,
    member(Value1,[1,2,3,4,5,6,7]),
    member(Value2,[2,3,4,5,6,7,8]),
    member(Value3,[3,4,5,6,7,8,9]),
    Value1 < Value2,
    Value2 < Value3,
    member(Coor1, Region),
    member(Coor2, Region),
    member(Coor3, Region),
    Coor1 = coor(X1,Y1),
    Coor2 = coor(X2,Y2),
    Coor3 = coor(X3,Y3),
    Sum1 #= X1 + 9 * Y1,
    Sum2 #= X2 + 9 * Y2,
    Sum3 #= X3 + 9 * Y3,
    Sum1 #< Sum2,
    Sum2 #< Sum3,
    substract(Region, [Coor1,Coor2,Coor3], List),
    where(Rows, List, Value1, []),
    where(Rows, List, Value2, []),
    where(Rows, List, Value3, []),
    cell(Rows, Coor1, Cell1),
    cell(Rows, Coor2, Cell2),
    cell(Rows, Coor3, Cell3),
	append(Cell1, Cell2, Cell12),
	append(Cell12, Cell3, Cell123),
	intersect([Value1,Value2,Value3], Cell123, [Value1,Value2,Value3]),
	intersect(Cell1, [Value1,Value2,Value3], [_|[_|_]]),
	intersect(Cell2, [Value1,Value2,Value3], [_|[_|_]]),
	intersect(Cell3, [Value1,Value2,Value3], [_|[_|_]]),
    substract(Cell1, [Value1,Value2,Value3], Values1),
    substract(Cell2, [Value1,Value2,Value3], Values2),
    substract(Cell3, [Value1,Value2,Value3], Values3),
    findall([[Coor1],Value], member(Value,Values1),All1),
    findall([[Coor2],Value], member(Value,Values2),All2),
    findall([[Coor3],Value], member(Value,Values3),All3),
    append(All1,All2,All12),
    append(All12,All3,Coors),
    \+Coors = [],!.

%
%===X Wing===
%

x_wing_list(Rows, L):-
    findall([Coors,Value],x_wing(Rows, Coors,Value), L).

x_wing(Rows, Coors, Value):-
    member(Value, [1,2,3,4,5,6,7,8,9]),
    col_coors(C1, Col1),
    col_coors(C2, Col2),
    C1<C2,
    row_coors(R1, Row1),
    row_coors(R2, Row2),
	R1<R2,
	cell(Rows, coor(C1,R1), Cell1),
    member(Value, Cell1),
    cell(Rows, coor(C1,R2), Cell2),
    member(Value, Cell2),
    cell(Rows, coor(C2,R1), Cell3),
    member(Value, Cell3),
    cell(Rows, coor(C2,R2), Cell4),
    member(Value, Cell4),
    where(Rows, Col1, Value, [coor(C1,R1),coor(C1,R2)]),
    where(Rows, Col2, Value, [coor(C2,R1),coor(C2,R2)]),
    where(Rows, Row1, Value, Coor1),
    where(Rows, Row2, Value, Coor2),
    append(Coor1,Coor2,Coor12),
    substract(Coor12,[coor(C1,R1),coor(C1,R2),coor(C2,R1),coor(C2,R2)], Coors),
    Coors \= [].

x_wing(Rows, Coors, Value):-
    member(Value, [1,2,3,4,5,6,7,8,9]),
    col_coors(C1, Col1),
    col_coors(C2, Col2),
    C1<C2,
    row_coors(R1, Row1),
    row_coors(R2, Row2),
	R1<R2,
	cell(Rows, coor(C1,R1), Cell1),
    member(Value, Cell1),
    cell(Rows, coor(C1,R2), Cell2),
    member(Value, Cell2),
    cell(Rows, coor(C2,R1), Cell3),
    member(Value, Cell3),
    cell(Rows, coor(C2,R2), Cell4),
    member(Value, Cell4),
    where(Rows, Row1, Value, [coor(C1,R1),coor(C2,R1)]),
    where(Rows, Row2, Value, [coor(C1,R2),coor(C2,R2)]),
    where(Rows, Col1, Value, Coor1),
    where(Rows, Col2, Value, Coor2),
    append(Coor1,Coor2,Coor12),
    substract(Coor12,[coor(C1,R1),coor(C1,R2),coor(C2,R1),coor(C2,R2)], Coors),
    Coors \= [].

%
%===Naked Quadruple===
%

naked_quadruple_list(Rows, L):-
    findall(Coors,naked_quadruple(Rows, Coors, _,_), List),
    flat(List,FlatList),
    rm_empty_coors(FlatList,L).

naked_quadruple(Rows, Coors, Value1, Value2):-
    row_coors(_, Region),
    member(Value1,[1,2,3,4,5,6]),
    member(Value2,[2,3,4,5,6,7]),
    member(Value3,[3,4,5,6,7,8]),
    member(Value4,[4,5,6,7,8,9]),
    Value1 < Value2,
    Value2 < Value3,
    Value3 < Value4,
    member(Coor1, Region),
    member(Coor2, Region),
    member(Coor3, Region),
    member(Coor4, Region),
    Coor1 = coor(X1,Y1),
    Coor2 = coor(X2,Y2),
    Coor3 = coor(X3,Y3),
    Coor4 = coor(X4,Y4),
    Sum1 #= X1 + 9 * Y1,
    Sum2 #= X2 + 9 * Y2,
    Sum3 #= X3 + 9 * Y3,
    Sum4 #= X4 + 9 * Y4,
    Sum1 #< Sum2,
    Sum2 #< Sum3,
    Sum3 #< Sum4,
    cell(Rows, Coor1, Pencils1),
    substract(Pencils1, [Value1,Value2,Value3, Value4],[]),
    cell(Rows, Coor2, Pencils2),
    substract(Pencils2, [Value1,Value2,Value3, Value4],[]),
    cell(Rows, Coor3, Pencils3),
    substract(Pencils3, [Value1,Value2,Value3, Value4],[]),
    cell(Rows, Coor4, Pencils4),
    substract(Pencils4, [Value1,Value2,Value3, Value4],[]),
    substract(Region, [Coor1,Coor2,Coor3,Coor4], List),
    where(Rows, List, Value1, Coors1),
    where(Rows, List, Value2, Coors2),
    where(Rows, List, Value3, Coors3),
    where(Rows, List, Value4, Coors4),
    not_all_empty(Coors1,Coor2,Coors3,Coors4),
    Coors = [[Coors1, Value1],[Coors2,Value2],[Coors3,Value3],[Coors4,Value4]].

naked_quadruple(Rows, Coors, Value1, Value2):-
    col_coors(_, Region),
    member(Value1,[1,2,3,4,5,6]),
    member(Value2,[2,3,4,5,6,7]),
    member(Value3,[3,4,5,6,7,8]),
    member(Value4,[4,5,6,7,8,9]),
    Value1 < Value2,
    Value2 < Value3,
    Value3 < Value4,
    member(Coor1, Region),
    member(Coor2, Region),
    member(Coor3, Region),
    member(Coor4, Region),
    Coor1 = coor(X1,Y1),
    Coor2 = coor(X2,Y2),
    Coor3 = coor(X3,Y3),
    Coor4 = coor(X4,Y4),
    Sum1 #= X1 + 9 * Y1,
    Sum2 #= X2 + 9 * Y2,
    Sum3 #= X3 + 9 * Y3,
    Sum4 #= X4 + 9 * Y4,
    Sum1 #< Sum2,
    Sum2 #< Sum3,
    Sum3 #< Sum4,
    cell(Rows, Coor1, Pencils1),
    substract(Pencils1, [Value1,Value2,Value3, Value4],[]),
    cell(Rows, Coor2, Pencils2),
    substract(Pencils2, [Value1,Value2,Value3, Value4],[]),
    cell(Rows, Coor3, Pencils3),
    substract(Pencils3, [Value1,Value2,Value3, Value4],[]),
    cell(Rows, Coor4, Pencils4),
    substract(Pencils4, [Value1,Value2,Value3, Value4],[]),
    substract(Region, [Coor1,Coor2,Coor3,Coor4], List),
    where(Rows, List, Value1, Coors1),
    where(Rows, List, Value2, Coors2),
    where(Rows, List, Value3, Coors3),
    where(Rows, List, Value4, Coors4),
    not_all_empty(Coors1,Coor2,Coors3,Coors4),
    Coors = [[Coors1, Value1],[Coors2,Value2],[Coors3,Value3],[Coors4,Value4]].

naked_quadruple(Rows, Coors, Value1, Value2):-
    block_coors(_,_, Region),
    member(Value1,[1,2,3,4,5,6]),
    member(Value2,[2,3,4,5,6,7]),
    member(Value3,[3,4,5,6,7,8]),
    member(Value4,[4,5,6,7,8,9]),
    Value1 < Value2,
    Value2 < Value3,
    Value3 < Value4,
    member(Coor1, Region),
    member(Coor2, Region),
    member(Coor3, Region),
    member(Coor4, Region),
    Coor1 = coor(X1,Y1),
    Coor2 = coor(X2,Y2),
    Coor3 = coor(X3,Y3),
    Coor4 = coor(X4,Y4),
    Sum1 #= X1 + 9 * Y1,
    Sum2 #= X2 + 9 * Y2,
    Sum3 #= X3 + 9 * Y3,
    Sum4 #= X4 + 9 * Y4,
    Sum1 #< Sum2,
    Sum2 #< Sum3,
    Sum3 #< Sum4,
    cell(Rows, Coor1, Pencils1),
    substract(Pencils1, [Value1,Value2,Value3, Value4],[]),
    cell(Rows, Coor2, Pencils2),
    substract(Pencils2, [Value1,Value2,Value3, Value4],[]),
    cell(Rows, Coor3, Pencils3),
    substract(Pencils3, [Value1,Value2,Value3, Value4],[]),
    cell(Rows, Coor4, Pencils4),
    substract(Pencils4, [Value1,Value2,Value3, Value4],[]),
    substract(Region, [Coor1,Coor2,Coor3,Coor4], List),
    where(Rows, List, Value1, Coors1),
    where(Rows, List, Value2, Coors2),
    where(Rows, List, Value3, Coors3),
    where(Rows, List, Value4, Coors4),
    not_all_empty(Coors1,Coor2,Coors3,Coors4),
    Coors = [[Coors1, Value1],[Coors2,Value2],[Coors3,Value3],[Coors4,Value4]].

not_all_empty(_,_,_,[_|_]):-!.
not_all_empty(_,_,[_|_],_):-!.
not_all_empty(_,[_|_],_,_):-!.
not_all_empty([_|_],_,_,_):-!.

%
%===Hidden Quadruple===
%

hidden_quadruple_list(Rows, L):-
    findall(Coors,hidden_quadruple(Rows, Coors, _,_), List),
    flat(List,FlatList),
    rm_empty_coors(FlatList,L).

hidden_quadruple(Rows, Coors, Value1, Value2):-
    row_coors(_, Row),
    count_emptys(Rows, Row, Region),
	length(Region, Count),
    Count > 5 ,
    member(Value1,[1,2,3,4,5,6]),
    member(Value2,[2,3,4,5,6,7]),
    member(Value3,[3,4,5,6,7,8]),
    member(Value4,[4,5,6,7,8,9]),
    Value1 < Value2,
    Value2 < Value3,
    Value3 < Value4,
    member(Coor1, Region),
    member(Coor2, Region),
    member(Coor3, Region),
    member(Coor4, Region),
    Coor1 = coor(X1,Y1),
    Coor2 = coor(X2,Y2),
    Coor3 = coor(X3,Y3),
    Coor4 = coor(X4,Y4),
    Sum1 #= X1 + 9 * Y1,
    Sum2 #= X2 + 9 * Y2,
    Sum3 #= X3 + 9 * Y3,
    Sum4 #= X4 + 9 * Y4,
    Sum1 #< Sum2,
    Sum2 #< Sum3,
    Sum3 #< Sum4,
    substract(Region, [Coor1,Coor2,Coor3,Coor4], List),
    where(Rows, List, Value1, []),
    where(Rows, List, Value2, []),
    where(Rows, List, Value3, []),
    where(Rows, List, Value4, []),
    cell(Rows, Coor1, Cell1),
    cell(Rows, Coor2, Cell2),
    cell(Rows, Coor3, Cell3),
    cell(Rows, Coor4, Cell4),
	append(Cell1, Cell2, Cell12),
	append(Cell12, Cell3, Cell123),
	append(Cell123, Cell4, Cell1234),
	intersect([Value1,Value2,Value3,Value4], Cell1234, [Value1,Value2,Value3,Value4]),
	intersect(Cell1, [Value1,Value2,Value3,Value4], [_|[_|_]]),
	intersect(Cell2, [Value1,Value2,Value3,Value4], [_|[_|_]]),
	intersect(Cell3, [Value1,Value2,Value3,Value4], [_|[_|_]]),
    intersect(Cell4, [Value1,Value2,Value3,Value4], [_|[_|_]]),
    substract(Cell1, [Value1,Value2,Value3,Value4], Values1),
    substract(Cell2, [Value1,Value2,Value3,Value4], Values2),
    substract(Cell3, [Value1,Value2,Value3,Value4], Values3),
    substract(Cell4, [Value1,Value2,Value3,Value4], Values4),
    findall([[Coor1],Value], member(Value,Values1),All1),
    findall([[Coor2],Value], member(Value,Values2),All2),
    findall([[Coor3],Value], member(Value,Values3),All3),
    findall([[Coor4],Value], member(Value,Values4),All4),
    append(All1,All2,All12),
    append(All12,All3,All123),
    append(All123,All4,Coors),
    \+Coors = [],!.

hidden_quadruple(Rows, Coors, Value1, Value2):-
    col_coors(_, Row),
    count_emptys(Rows, Row, Region),
	length(Region, Count),
    Count > 5 ,
    member(Value1,[1,2,3,4,5,6]),
    member(Value2,[2,3,4,5,6,7]),
    member(Value3,[3,4,5,6,7,8]),
    member(Value4,[4,5,6,7,8,9]),
    Value1 < Value2,
    Value2 < Value3,
    Value3 < Value4,
    member(Coor1, Region),
    member(Coor2, Region),
    member(Coor3, Region),
    member(Coor4, Region),
    Coor1 = coor(X1,Y1),
    Coor2 = coor(X2,Y2),
    Coor3 = coor(X3,Y3),
    Coor4 = coor(X4,Y4),
    Sum1 #= X1 + 9 * Y1,
    Sum2 #= X2 + 9 * Y2,
    Sum3 #= X3 + 9 * Y3,
    Sum4 #= X4 + 9 * Y4,
    Sum1 #< Sum2,
    Sum2 #< Sum3,
    Sum3 #< Sum4,
    substract(Region, [Coor1,Coor2,Coor3,Coor4], List),
    where(Rows, List, Value1, []),
    where(Rows, List, Value2, []),
    where(Rows, List, Value3, []),
    where(Rows, List, Value4, []),
    cell(Rows, Coor1, Cell1),
    cell(Rows, Coor2, Cell2),
    cell(Rows, Coor3, Cell3),
    cell(Rows, Coor4, Cell4),
	append(Cell1, Cell2, Cell12),
	append(Cell12, Cell3, Cell123),
	append(Cell123, Cell4, Cell1234),
	intersect([Value1,Value2,Value3,Value4], Cell1234, [Value1,Value2,Value3,Value4]),
	intersect(Cell1, [Value1,Value2,Value3,Value4], [_|[_|_]]),
	intersect(Cell2, [Value1,Value2,Value3,Value4], [_|[_|_]]),
	intersect(Cell3, [Value1,Value2,Value3,Value4], [_|[_|_]]),
    intersect(Cell4, [Value1,Value2,Value3,Value4], [_|[_|_]]),
    substract(Cell1, [Value1,Value2,Value3,Value4], Values1),
    substract(Cell2, [Value1,Value2,Value3,Value4], Values2),
    substract(Cell3, [Value1,Value2,Value3,Value4], Values3),
    substract(Cell4, [Value1,Value2,Value3,Value4], Values4),
    findall([[Coor1],Value], member(Value,Values1),All1),
    findall([[Coor2],Value], member(Value,Values2),All2),
    findall([[Coor3],Value], member(Value,Values3),All3),
    findall([[Coor4],Value], member(Value,Values4),All4),
    append(All1,All2,All12),
    append(All12,All3,All123),
    append(All123,All4,Coors),
    \+Coors = [],!.

hidden_quadruple(Rows, Coors, Value1, Value2):-
    block_coors(_,_, Row),
    count_emptys(Rows, Row, Region),
	length(Region, Count),
    Count > 5 ,
    member(Value1,[1,2,3,4,5,6]),
    member(Value2,[2,3,4,5,6,7]),
    member(Value3,[3,4,5,6,7,8]),
    member(Value4,[4,5,6,7,8,9]),
    Value1 < Value2,
    Value2 < Value3,
    Value3 < Value4,
    member(Coor1, Region),
    member(Coor2, Region),
    member(Coor3, Region),
    member(Coor4, Region),
    Coor1 = coor(X1,Y1),
    Coor2 = coor(X2,Y2),
    Coor3 = coor(X3,Y3),
    Coor4 = coor(X4,Y4),
    Sum1 #= X1 + 9 * Y1,
    Sum2 #= X2 + 9 * Y2,
    Sum3 #= X3 + 9 * Y3,
    Sum4 #= X4 + 9 * Y4,
    Sum1 #< Sum2,
    Sum2 #< Sum3,
    Sum3 #< Sum4,
    substract(Region, [Coor1,Coor2,Coor3,Coor4], List),
    where(Rows, List, Value1, []),
    where(Rows, List, Value2, []),
    where(Rows, List, Value3, []),
    where(Rows, List, Value4, []),
    cell(Rows, Coor1, Cell1),
    cell(Rows, Coor2, Cell2),
    cell(Rows, Coor3, Cell3),
    cell(Rows, Coor4, Cell4),
	append(Cell1, Cell2, Cell12),
	append(Cell12, Cell3, Cell123),
	append(Cell123, Cell4, Cell1234),
	intersect([Value1,Value2,Value3,Value4], Cell1234, [Value1,Value2,Value3,Value4]),
	intersect(Cell1, [Value1,Value2,Value3,Value4], [_|[_|_]]),
	intersect(Cell2, [Value1,Value2,Value3,Value4], [_|[_|_]]),
	intersect(Cell3, [Value1,Value2,Value3,Value4], [_|[_|_]]),
    intersect(Cell4, [Value1,Value2,Value3,Value4], [_|[_|_]]),
    substract(Cell1, [Value1,Value2,Value3,Value4], Values1),
    substract(Cell2, [Value1,Value2,Value3,Value4], Values2),
    substract(Cell3, [Value1,Value2,Value3,Value4], Values3),
    substract(Cell4, [Value1,Value2,Value3,Value4], Values4),
    findall([[Coor1],Value], member(Value,Values1),All1),
    findall([[Coor2],Value], member(Value,Values2),All2),
    findall([[Coor3],Value], member(Value,Values3),All3),
    findall([[Coor4],Value], member(Value,Values4),All4),
    append(All1,All2,All12),
    append(All12,All3,All123),
    append(All123,All4,Coors),
    \+Coors = [],!.

%
%===Swordfish===
%

swordfish_list(Rows, Sort):-
    findall(Coors,swordfish(Rows, Coors, _), L),
    sort(L, Sort).

sword_flat([],[]).

sword_flat([[First,Second]|Tail],[First,Second|FTail]):-
    sword_flat(Tail,FTail).

col_regions([],[]).

col_regions([coor(X,_)|Tail],Regions):-
    col_regions(Tail,RTail),
    col_coors(X,Col),
    append(Col,RTail,Regions).

row_regions([],[]).

row_regions([coor(_,Y)|Tail],Regions):-
    row_regions(Tail,RTail),
    row_coors(Y,Row),
    append(Row,RTail,Regions).

swordfish(Rows, Coors, Value):-
	member(Value,[1,2,3,4,5,6,7,8,9]),
    row_coors(_, Region),
    where(Rows, Region, Value, [Coor1,Coor2]),
    row_sword(Rows,Value,[[Coor1,Coor2]],Coor2,Sword),
    sword_flat(Sword,Flat),
    col_regions(Flat,Regions),
    sort(Regions, Sort),
    where(Rows, Sort, Value, Wheres),
    difference(Wheres,Flat,Diff),
    \+ Diff = [],
    Coors = [Diff,Value].

swordfish(Rows, Coors, Value):-
	member(Value,[1,2,3,4,5,6,7,8,9]),
    col_coors(_, Region),
    where(Rows, Region, Value, [Coor1,Coor2]),
    col_sword(Rows,Value,[[Coor1,Coor2]],Coor2,Sword),
    sword_flat(Sword,Flat),
    row_regions(Flat,Regions),
    sort(Regions, Sort),
    where(Rows, Sort, Value, Wheres),
    difference(Wheres,Flat,Diff),
    \+ Diff = [],
    Coors = [Diff,Value].

row_sword(_, _,  [[coor(X,Y),Second]|Tail],coor(X,_), [[coor(X,Y),Second]|Tail]):-!.

row_sword(Rows, Value, [[coor(HeadX,HeadY),coor(TailX,TailY)]|Tail],Last,Sword):-
    length([[coor(HeadX,HeadY),coor(TailX,TailY)]|Tail], Len),
    Len < 9,
    %print("Row_sword: "),print([[coor(HeadX,HeadY),coor(TailX,TailY)]|Tail]),print("   End Line"),nl(),
    row_coors(_, Region),
    where(Rows, Region, Value, [coor(HeadX,Y1),coor(X2,Y2)]),
    \+Y1 = HeadY,
    row_sword(Rows,Value,[[coor(X2,Y2),coor(HeadX,Y1)],[coor(HeadX,HeadY),coor(TailX,TailY)]|Tail], Last,Sword).

row_sword(Rows, Value, [[coor(HeadX,HeadY),coor(TailX,TailY)]|Tail],Last,Sword):-
    length([[coor(HeadX,HeadY),coor(TailX,TailY)]|Tail], Len),
    Len < 9,
    %print("Row_sword: "),print([[coor(HeadX,HeadY),coor(TailX,TailY)]|Tail]),print("   End Line"),nl(),
    row_coors(_, Region),
    where(Rows, Region, Value, [coor(X2,Y2),coor(HeadX,Y1)]),
    \+Y1 = HeadY,
    row_sword(Rows,Value,[[coor(X2,Y2),coor(HeadX,Y1)],[coor(HeadX,HeadY),coor(TailX,TailY)]|Tail], Last,Sword).

col_sword(_, _, [[coor(X,Y),Second]|Tail],coor(_,Y), [[coor(X,Y),Second]|Tail]):-!.

col_sword(Rows, Value, [[coor(HeadX,HeadY),coor(TailX,TailY)]|Tail],Last,Sword):-
    length([[coor(HeadX,HeadY),coor(TailX,TailY)]|Tail], Len),
    Len < 9,
    %print("Col_sword"),print([[coor(HeadX,HeadY),coor(TailX,TailY)]|Tail]),print("   End Line"),nl(),
    col_coors(_, Region),
    where(Rows, Region, Value, [coor(X1,HeadY),coor(X2,Y2)]),
    \+X1 = HeadX,
    col_sword(Rows,Value,[[coor(X2,Y2),coor(X1,HeadY)],[coor(HeadX,HeadY),coor(TailX,TailY)]|Tail], Last,Sword).

col_sword(Rows, Value, [[coor(HeadX,HeadY),coor(TailX,TailY)]|Tail],Last,Sword):-
    length([[coor(HeadX,HeadY),coor(TailX,TailY)]|Tail], Len),
    Len < 9,
    %print("Col_sword"),print([[coor(TailX,TailY),coor(HeadX,HeadY)]|Tail]),print("   End Line"),nl(),
    col_coors(_, Region),
    where(Rows, Region, Value, [coor(X1,HeadY),coor(X2,Y2)]),
    \+X1 = HeadX,
    col_sword(Rows,Value,[[coor(X2,Y2),coor(X1,HeadY)],[coor(HeadX,HeadY),coor(TailX,TailY)]|Tail], Last,Sword).

%[ [[coor(5, 3)], 2],
%  [[coor(3, 3), coor(5, 3)], 3],
%  [[coor(3, 3), coor(7, 3)], 9],
%  [[coor(3, 0)], 5]],

%problem(6, Rows),
%V = 8,
%cell(Rows, coor(5,7), Cell),
%findallPencils(Rows, 3, New),
%    row_coors(_, Row),
%    block_coors(1, 2, B),
%    where(Rows, B, V, L),
%    sublist(L, Row),
%    \+ L = [],
%    substract(Row, L, List),
%    where(Rows, List, V, Coors),
%    \+ Coors = [], print(V).

