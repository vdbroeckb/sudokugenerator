from pyswip import *
import inspect
from ..Sudoku import *
from ..Tile import *
from ..SudokuDrawWindow import *
from ..commands.DeletePencil import DeletePencil

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)


sudoku = Sudoku()
# file = "../../sudokus/test"
file = "../../sudokus/s00A"
sudoku.parseFromFile(file, " ")
sudoku.printText()
sudoDraw = SudokuDrawWindow(600, True, True)
if sudoDraw:
    sudoDraw.drawSudoku(sudoku)
    sudoDraw.pauseUntilClick()
# sudoku.generatePencilMarks()
# if sudoDraw:
#    sudoDraw.drawSudoku(sudoku)
#    sudoDraw.pauseUntilClick()
print(sudoku.toPl())
prolog = Prolog()
prolog.consult("Tester.pl")

problem = Functor("problem", 2)
a = "problem(1," + sudoku.toPl() + ")"
prolog.assertz(a)
# prolog.assertz("problem(1, [[_,_,_,_,_,_,_,_,_],[_,_,_,_,_,3,_,8,5],[_,_,1,_,2,_,_,_,_],[_,_,_,5,_,7,_,_,_],[_,_,4,_,_,_,1,_,_],[_,9,_,_,_,_,_,_,_],[5,_,_,_,_,_,_,7,3],[_,_,2,_,1,_,_,_,_],[_,_,_,_,4,_,_,_,9]])")
newSudoku = Sudoku()
for soln in prolog.query("problem(1, Rows), fill_pencils(Rows,Rows,Pencils,0).", 1):
    # print(soln["X"])
    newSudoku.matrix = [[Tile(i, set()) if isinstance(i, int) else Tile(0, set(i))for i in j] for j in soln["Pencils"]]
    print(soln["Pencils"])
    # for j in soln["Rows"]:
    #    for i in j:
    #        print(i)

newSudoku.printText()
if sudoDraw:
    sudoDraw.drawSudoku(newSudoku)
    sudoDraw.pauseUntilClick()


newSudoku = Sudoku()
for soln in prolog.query("problem(6, Pencils).", 1):
    # print(soln["X"])
    newSudoku.matrix = [[Tile(i, set()) if isinstance(i, int) else Tile(0, set(i))for i in j] for j in soln["Pencils"]]
    print(soln["Pencils"])
    # for j in soln["Rows"]:
    #    for i in j:
    #        print(i)

if sudoDraw:
    sudoDraw.clearHighlights()
    sudoDraw.clearNumbers()
    sudoDraw.clearPencilMarks()
    sudoDraw.drawSudoku(newSudoku)
    sudoDraw.pauseUntilClick()

commandSet = set()
for soln in prolog.query("problem(6, Rows), naked_pair_list(Rows, List)."):
    print("List =" + str(soln["List"]))
    for array in soln["List"]:
        for coor in array[0]:
            command = DeletePencil(coor.args[0], coor.args[1], array[1])
            command.print()
            commandSet.add(command)

if sudoDraw:
    sudoDraw.clearHighlights()
    sudoDraw.clearNumbers()
    sudoDraw.clearPencilMarks()
    sudoDraw.drawSudoku(newSudoku)
    sudoDraw.pauseUntilClick()
