from .algorithms.SinglePositionAlgorithm import *
from .algorithms.SingleCandidateAlgorithm import *
from .algorithms.XWingAlgorithm import *
from .algorithms.NakedQuadrupletsAlgorithm import *
from .algorithms.HiddenQuadrupletsAlgorithm import *
from .algorithms.SwordFishAlgorithm import *
from .inverters.EasyInverter import *
from .inverters.MediumInverter import *
from .inverters.DifficultInverter import *
from .commands.CommandQueue import *
from queue import *

from .Checker import *
import time


class SudokuSolver:
    algorithms = [SinglePositionAlgorithm,
                  SingleCandidateAlgorithm,
                  CandidateLinesAlgorithm,
                  MultipleLinesAlgorithm,
                  NakedPairsAlgorithm,
                  NakedTriplesAlgorithm,
                  HiddenPairsAlgorithm,
                  HiddenTriplesAlgorithm,
                  XWingAlgorithm,
                  NakedQuadrupletsAlgorithm,
                  HiddenQuadrupletsAlgorithm,
                  SwordFishAlgorithm]

    @staticmethod
    def solveAndGetDifficulty(sudoku, debugDraw, maxDifficulty=100):
        algorithms = SudokuSolver.algorithms
        sudoku.generatePencilMarks()

        debugDraw.drawSudoku(sudoku)

        currentDifficulty = 1
        maxReachedDifficulty = currentDifficulty

        while currentDifficulty <= maxDifficulty and currentDifficulty <= len(algorithms):
            commandSet = algorithms[currentDifficulty - 1].execute(sudoku)
            for command in commandSet:
                maxReachedDifficulty = max(maxReachedDifficulty, currentDifficulty)

                debugDraw.clearAndDraw(sudoku)
                command.draw(debugDraw)

                command.execute(sudoku)

                debugDraw.clearAndDraw(sudoku)
                command.draw(debugDraw)

                currentDifficulty = 1

            if Checker.isSolved(sudoku):
                return maxReachedDifficulty
            if not len(commandSet):
                currentDifficulty += 1
            maxReachedDifficulty = max(maxReachedDifficulty, currentDifficulty)

        print("Sudoku not solvable.")

        return -1

    @staticmethod
    def invertSolveEasy(sudoku, maxtime, debugDraw):
        return SudokuSolver.invert(sudoku, maxtime, EasyInverter(), debugDraw)

    @staticmethod
    def invertSolveMedium(sudoku, maxtime, debugDraw):
        return SudokuSolver.invert(sudoku, maxtime, MediumInverter(), debugDraw)

    @staticmethod
    def invertSolveDifficult(sudoku, maxtime, debugDraw):
        return SudokuSolver.invert(sudoku, maxtime, DifficultInverter(), debugDraw)

    @staticmethod
    def invert(sudoku, maxtime, inverter, debugDraw):
        debugIterator = 0
        copySudoku = sudoku.copy()
        visited = set()

        start_time = time.time()
        pQ = PriorityQueue()

        command = FillPencil(1, 1, sudoku.getTile(1, 1).value)

        cQ = CommandQueue([command])
        requirements = inverter.requirements(sudoku, 1, 1, sudoku.getTile(1, 1).value)
        for i in requirements:
            cQ.toBreakRequirements.add(i)
        pQ.put(cQ)
        visited.add(cQ)

        found = False
        while not found:
            if time.time() - start_time > maxtime:
                # print(" difficulty = ," + str(0) + ", time =," + str(
                #    time.time() - start_time) + ", Iterations= ," + str(debugIterator))
                return None, debugIterator

            cQ = pQ.get()
            #print(len(cQ.toBreakRequirements))
            debugIterator += 1
            cQ.iter = debugIterator
            cQ.execute(sudoku)

            debugDraw.clearAndDraw(sudoku)
            debugDraw.highlightTile(command.column, command.row)

            if len(cQ.toBreakRequirements):
                npaSet = inverter.inverseExecute(sudoku, cQ)
                for newQ in npaSet:
                    if newQ not in visited:
                        visited.add(newQ)
                        pQ.put(newQ)
                sudoku = copySudoku.copy()
            else:
                return sudoku, debugIterator



    @staticmethod
    def commandsInfluence(sudoku, first, second):
        return first.column == second.column \
               or first.row == second.row \
               or (first.row // sudoku.dim == second.row // sudoku.dim
                   and first.column // sudoku.dim == second.column // sudoku.dim)
