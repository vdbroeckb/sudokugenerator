from .Sudoku import *


class SudokuParser:

    @staticmethod
    def parseFromFile(path, separator=" "):
        file = open(path, "r")
        sudoku = SudokuParser.parse("".join(file.readlines()), separator)
        file.close()
        return sudoku

    @staticmethod
    def parse(string, separator):
        string = string.replace("\n", separator)

        line = string.split(separator)
        line = list(filter(None, line))  # Filter empty strings
        line = list(map(lambda x: int(x), line))
        sudoku = Sudoku()
        sudoku.matrix = SudokuParser.listToMatrix(line)
        sudoku.setTiles()

        return sudoku

    @staticmethod
    def listToMatrix(l):
        matrix = []
        matrixDim = math.sqrt(len(l))
        currentCol = []

        for i in range(len(l)):
            if i % matrixDim == 0:
                currentCol = []
                matrix.append(currentCol)
            if l[i] == 0:
                pencilMarks = set()
                currentCol.append(Tile(0, pencilMarks))
            else:
                currentCol.append(Tile(l[i], set()))

        return matrix

    @staticmethod
    def toText(sudoku):
        s = ""
        for i in range(sudoku.dim ** 2):
            for tile in sudoku.getColumn(i):
                if tile.value:
                    s += str(tile.value) + " "
                else:
                    s += "0 "
            s += "\n"
        return s

    @staticmethod
    def toLatex(sudoku):
        showPencilMarks = False

        retString = ""
        retString += "\\begin{tikzpicture}\n"
        retString += "  \\begin{scope}[scale=.75]\n"
        retString += "    \\draw[thin,gray!20] (0, 0) grid (9, 9);\n"
        retString += "    \\draw[scale=3] (0, 0) grid (3, 3);\n\n"

        retString += "    %cijfers\n"
        retString += "    \\setcounter{row}{1}\n"

        for i in range(self.dim ** 2):
            retString += "    \\setrow "
            for j in sudoku.getRowValues(i):
                retString += "{"
                retString += str(j) if j else " "
                retString += "}"
            retString += "\n"

        if showPencilMarks:
            retString += "\n\n    %pencilmarks\n"
            for rowIterator in range(sudoku.dim ** 2):
                for columnIterator in range(sudoku.dim ** 2):
                    if sudoku.matrix[columnIterator][rowIterator].isEmpty():
                        retString += "    \\hintcell"
                        retString += "{" + str(rowIterator + 1) + "}"
                        retString += '{' + str(columnIterator + 1) + "}{"
                        for i in sudoku.matrix[columnIterator][rowIterator].getPencilMarks():
                            retString += str(i)
                            retString += ","
                        retString = retString[:-1]
                        retString += "}\n"
        retString += "\\end{scope}\n"
        retString += "\\end{tikzpicture}\n"
        return retString

    @staticmethod
    def toLatexBoekje(sudoku):
        retString = ""
        retString += "\\begin{scope}[xshift=10cm,\n              yshift=10cm," \
                     "\n              rotate=0," \
                     "\n              MyRotate/.style={" \
                     "\n              rotate=0," \
                     "\n              scale=2}," \
                     "\n              scale=2]\n"
        retString += "  \\draw[fill = white] (0, 0) rectangle(9, 9);\n"
        retString += "    \\draw[thin,gray!20] (0, 0) grid (9, 9);\n"
        retString += "    \\draw[scale=3] (0, 0) grid (3, 3);\n\n"

        retString += "    %cijfers\n"
        retString += "    \\setcounter{row}{1}\n"

        for i in range(sudoku.dim ** 2):
            retString += "    \\setrow "
            for j in sudoku.getRowValues(i):
                retString += "{"
                retString += str(j) if j else " "
                retString += "}"
            retString += "\n"
        retString += "\n\n    %pencilmarks\n"
        for rowIterator in range(sudoku.dim ** 2):
            for columnIterator in range(sudoku.dim ** 2):
                if sudoku.matrix[columnIterator][rowIterator].isEmpty():
                    retString += "    \\hintcell"
                    retString += "{" + str(rowIterator + 1) + "}"
                    retString += '{' + str(columnIterator + 1) + "}{"
                    for i in sudoku.matrix[columnIterator][rowIterator].getPencilMarks():
                        retString += str(i)
                        retString += ","
                    retString = retString[:-1]
                    retString += "}\n"

        retString += "\\end{scope}\n"
        return retString

