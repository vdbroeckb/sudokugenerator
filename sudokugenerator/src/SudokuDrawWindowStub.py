class SudokuDrawWindowStub:

    def __init__(self, _size=600, _drawsEmpty=True, _drawsPencilMark=False):
        pass

    def clearNumbers(self):
        pass

    def clearPencilMarks(self):
        pass

    def highlightTile(self, col, row, color="red", width=4):
        pass

    def clearHighlights(self):
        pass

    def selectRow(self, row):
        pass

    def selectColumn(self, col):
        pass

    def selectTile(self, col, row):
        pass

    def selectBox(self, col, row):
        pass

    def drawSudoku(self, sudoku):
        pass

    def drawNumbers(self, sudoku):
        pass

    def drawGrid(self):
        pass

    def drawPencilMark(self, col, row, pencilMark):
        pass

    def pauseUntilClick(self):
        pass

    def close(self):
        pass

    def clearAndDraw(self, sudoku):
        pass
