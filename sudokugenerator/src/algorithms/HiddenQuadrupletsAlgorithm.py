from ..commands.DeletePencil import DeletePencil
from pyswip import *


class HiddenQuadrupletsAlgorithm:
    difficulty = 11

    @staticmethod
    def execute(sudoku):
        commandSet = set()

        return commandSet

    @staticmethod
    def where(value, s):
        return {i for i in s if value in i.pencilMarks}

    @staticmethod
    def executePl(sudoku):
        print("execute HQA Pl")
        commandSet = set()
        pl = Prolog()
        pl.consult("src/Pl/Tester.pl")
        query = "hidden_quadruple_list(" + sudoku.toPl() + " , List)."
        for soln in pl.query(query):
            for array in soln["List"]:
                for coor in array[0]:
                    command = DeletePencil(coor.args[0], coor.args[1], array[1])
                    command.print()
                    commandSet.add(command)
        return commandSet
