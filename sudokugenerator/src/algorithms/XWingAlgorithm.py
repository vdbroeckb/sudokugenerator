from ..commands.DeletePencil import DeletePencil


class XWingAlgorithm:
    difficulty = 9

    @staticmethod
    def execute(sudoku):
        commandSet = set()
        for firstCol in range(sudoku.dim ** 2 - 1):
            fC = sudoku.getColumn(firstCol)
            for value in range(1, 1 + sudoku.dim ** 2):
                fW = XWingAlgorithm.where(value, fC)
                if len(fW) == 2:
                    firstRows = {i.row for i in fW}
                    for secondCol in range(firstCol + 1, sudoku.dim ** 2):
                        sW = XWingAlgorithm.where(value, sudoku.getColumn(secondCol))
                        if len(sW) == 2:
                            secondRows = {i.row for i in sW}
                            if firstRows == secondRows:
                                for row in firstRows:
                                    for tile in sudoku.getRow(row):
                                        if tile.pencilContains(value) \
                                                and tile.column != firstCol and tile.column != secondCol:
                                            command = DeletePencil(tile.column, tile.row, value)
                                            commandSet.add(command)

        for firstRow in range(sudoku.dim ** 2 - 1):
            fR = sudoku.getRow(firstRow)
            for value in range(1, 1 + sudoku.dim ** 2):
                fW = XWingAlgorithm.where(value, fR)
                if len(fW) == 2:
                    firstColumns = {i.column for i in fW}
                    for secondRow in range(firstRow + 1, sudoku.dim ** 2):
                        sW = XWingAlgorithm.where(value, sudoku.getRow(secondRow))
                        if len(sW) == 2:
                            secondColumns = {i.column for i in sW}
                            if firstColumns == secondColumns:
                                for col in firstColumns:
                                    for tile in sudoku.getColumn(col):
                                        if tile.pencilContains(value) \
                                                and tile.row != firstRow and tile.row != secondRow:
                                            command = DeletePencil(tile.column, tile.row, value)
                                            commandSet.add(command)

        return commandSet

    @staticmethod
    def where(value, s):
        return {i for i in s if value in i.pencilMarks}
