from ..commands.DeletePencil import DeletePencil


class HiddenTriplesAlgorithm:
    difficulty = 8

    @staticmethod
    def execute(sudoku):
        commandSet = set()
        for colIterator in range(sudoku.dim ** 2):
            region = sudoku.getColumnEmptys(colIterator)
            if len(region) > 3:
                filled = sudoku.getColumnValuesSet(colIterator)
                for first in range(1, sudoku.dim ** 2 - 1):
                    if first not in filled:
                        fW = HiddenTriplesAlgorithm.where(first, region)
                        if len(fW) <= 3:
                            for second in range(first + 1, sudoku.dim ** 2):
                                if second not in filled:
                                    sW = HiddenTriplesAlgorithm.where(second, region)
                                    if len(sW) <= 3:
                                        for third in range(second + 1, sudoku.dim ** 2 + 1):
                                            tW = HiddenTriplesAlgorithm.where(third, region)
                                            if len(tW):
                                                coors = tW | sW | fW
                                                if len(coors) == 3:
                                                    for coor in coors:
                                                        for value in coor.pencilMarks:
                                                            if value != first and value != second and value != third:
                                                                command = DeletePencil(coor.column, coor.row, value)
                                                                commandSet.add(command)
        for RowIterator in range(sudoku.dim ** 2):
            region = sudoku.getRowEmptys(RowIterator)
            if len(region) > 3:
                filled = sudoku.getRowValuesSet(RowIterator)
                for first in range(1, sudoku.dim ** 2 - 1):
                    if first not in filled:
                        fW = HiddenTriplesAlgorithm.where(first, region)
                        if len(fW) <= 3:
                            for second in range(first + 1, sudoku.dim ** 2):
                                if second not in filled:
                                    sW = HiddenTriplesAlgorithm.where(second, region)
                                    if len(sW) <= 3:
                                        for third in range(second + 1, sudoku.dim ** 2 + 1):
                                            tW = HiddenTriplesAlgorithm.where(third, region)
                                            if len(tW):
                                                coors = tW | sW | fW
                                                if len(coors) == 3:
                                                    for coor in coors:
                                                        for value in coor.pencilMarks:
                                                            if value != first and value != second and value != third:
                                                                command = DeletePencil(coor.column, coor.row, value)
                                                                commandSet.add(command)
        for blockIterator in range(sudoku.dim ** 2):
            region = sudoku.getBlockEmptys(blockIterator // sudoku.dim, blockIterator % sudoku.dim)
            if len(region) > 3:
                filled = sudoku.getBlockValuesSet(blockIterator // sudoku.dim, blockIterator % sudoku.dim)
                for first in range(1, sudoku.dim ** 2 - 1):
                    if first not in filled:
                        fW = HiddenTriplesAlgorithm.where(first, region)
                        if len(fW) <= 3:
                            for second in range(first + 1, sudoku.dim ** 2):
                                if second not in filled:
                                    sW = HiddenTriplesAlgorithm.where(second, region)
                                    if len(sW) <= 3:
                                        for third in range(second + 1, sudoku.dim ** 2 + 1):
                                            tW = HiddenTriplesAlgorithm.where(third, region)
                                            if len(tW):
                                                coors = tW | sW | fW
                                                if len(coors) == 3:
                                                    for coor in coors:
                                                        for value in coor.pencilMarks:
                                                            if value != first and value != second and value != third:
                                                                command = DeletePencil(coor.column, coor.row, value)
                                                                commandSet.add(command)
        return commandSet

    @staticmethod
    def where(value, s):
        return {i for i in s if value in i.pencilMarks}
