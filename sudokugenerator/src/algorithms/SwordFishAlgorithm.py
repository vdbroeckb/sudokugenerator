import itertools
from ..commands.DeletePencil import DeletePencil


class SwordFishAlgorithm:
    difficulty = 12

    @staticmethod
    def execute(sudoku):
        commandSet = set()

        first3 = itertools.combinations(range(sudoku.dim ** 2), 3)

        for cols in first3:
            second3 = itertools.combinations(range(sudoku.dim ** 2), 3)
            for rows in second3:
                for n in range(sudoku.dim ** 2):
                    correct = True
                    for c in cols:
                        for tile in sudoku.getColumn(c):
                            if tile.isEmpty() and n in tile.getPencilMarks() and tile.getRow() not in rows:
                                correct = False
                                break
                    if correct:
                        for r in rows:
                            for tile in sudoku.getRow(r):
                                if tile.isEmpty() and n in tile.getPencilMarks() and tile.getColumn() not in cols:
                                    command = DeletePencil(tile.column, tile.row, n)
                                    commandSet.add(command)

        return commandSet

    @staticmethod
    def where(value, s):
        return {i for i in s if value in i.pencilMarks}
