from ..commands.DeletePencil import DeletePencil


class NakedQuadrupletsAlgorithm:
    difficulty = 10

    @staticmethod
    def execute(sudoku):
        commandSet = set()

        for colIterator in range(sudoku.dim ** 2):
            region = sudoku.getColumnEmptys(colIterator)
            if len(region) > 4:
                tp = NakedQuadrupletsAlgorithm.fourPencils(region)
                for i in range(len(tp) - 3):
                    for j in range(i + 1, len(tp) - 2):
                        for k in range(j + 1, len(tp) - 1):
                            for l in range(k + 1, len(tp)):
                                pencils = tp[i].pencilMarks | tp[j].pencilMarks | tp[k].pencilMarks | tp[l].pencilMarks
                                if len(pencils) == 4:
                                    for value in pencils:
                                        for tile in region:
                                            if tile.coors() != tp[i].coors() and \
                                                    tile.coors() != tp[j].coors() and \
                                                    tile.coors() != tp[k].coors() and \
                                                    tile.coors() != tp[l].coors() and value in tile.pencilMarks:
                                                command = DeletePencil(tile.column, tile.row, value)
                                                command.print()
                                                commandSet.add(command)

        for RowIterator in range(sudoku.dim ** 2):
            region = sudoku.getRowEmptys(RowIterator)
            if len(region) > 4:
                tp = NakedQuadrupletsAlgorithm.fourPencils(region)
                for i in range(len(tp) - 3):
                    for j in range(i + 1, len(tp) - 2):
                        for k in range(j + 1, len(tp) - 1):
                            for l in range(k + 1, len(tp)):
                                pencils = tp[i].pencilMarks | tp[j].pencilMarks | tp[k].pencilMarks | tp[l].pencilMarks
                                if len(pencils) == 4:
                                    for value in pencils:
                                        for tile in region:
                                            if tile.coors() != tp[i].coors() and \
                                                    tile.coors() != tp[j].coors() and \
                                                    tile.coors() != tp[k].coors() and \
                                                    tile.coors() != tp[l].coors() and value in tile.pencilMarks:
                                                command = DeletePencil(tile.column, tile.row, value)
                                                command.print()
                                                commandSet.add(command)

        for blockIterator in range(sudoku.dim ** 2):
            region = sudoku.getBlockEmptys(blockIterator // sudoku.dim, blockIterator % sudoku.dim)
            if len(region) > 4:
                tp = NakedQuadrupletsAlgorithm.fourPencils(region)
                for i in range(len(tp) - 3):
                    for j in range(i + 1, len(tp) - 2):
                        for k in range(j + 1, len(tp) - 1):
                            for l in range(k + 1, len(tp)):
                                pencils = tp[i].pencilMarks | tp[j].pencilMarks | tp[k].pencilMarks | tp[l].pencilMarks
                                if len(pencils) == 4:
                                    for value in pencils:
                                        for tile in region:
                                            if tile.coors() != tp[i].coors() and \
                                                    tile.coors() != tp[j].coors() and \
                                                    tile.coors() != tp[k].coors() and \
                                                    tile.coors() != tp[l].coors() and value in tile.pencilMarks:
                                                command = DeletePencil(tile.column, tile.row, value)
                                                command.print()
                                                commandSet.add(command)

        return commandSet

    @staticmethod
    def fourPencils(l):
        return [i for i in l if len(i.pencilMarks) <= 4]
