from ..commands.DeletePencil import DeletePencil


class NakedTriplesAlgorithm:
    difficulty = 6

    @staticmethod
    def execute(sudoku):
        commandSet = set()
        for colIterator in range(sudoku.dim ** 2):
            region = sudoku.getColumnEmptys(colIterator)
            if len(region) > 3:
                tp = NakedTriplesAlgorithm.threePencils(region)
                for i in range(len(tp)):
                    for j in range(i + 1, len(tp)):
                        for k in range(j + 1, len(tp)):
                            pencils = tp[i].pencilMarks | tp[j].pencilMarks | tp[k].pencilMarks
                            if len(pencils) == 3:
                                for value in pencils:
                                    for tile in region:
                                        if tile.coors() != tp[i].coors() and \
                                                tile.coors() != tp[j].coors() and \
                                                tile.coors() != tp[k].coors() and value in tile.pencilMarks:
                                            command = DeletePencil(tile.column, tile.row, value)
                                            commandSet.add(command)

        for RowIterator in range(sudoku.dim ** 2):
            region = sudoku.getRowEmptys(RowIterator)
            if len(region) > 3:
                tp = NakedTriplesAlgorithm.threePencils(region)
                for i in range(len(tp)):
                    for j in range(i + 1, len(tp)):
                        for k in range(j + 1, len(tp)):
                            pencils = tp[i].pencilMarks | tp[j].pencilMarks | tp[k].pencilMarks
                            if len(pencils) == 3:
                                for value in pencils:
                                    for tile in region:
                                        if tile.coors() != tp[i].coors() and \
                                                tile.coors() != tp[j].coors() and \
                                                tile.coors() != tp[k].coors() and value in tile.pencilMarks:
                                            command = DeletePencil(tile.column, tile.row, value)
                                            commandSet.add(command)

        for blockIterator in range(sudoku.dim ** 2):
            region = sudoku.getBlockEmptys(blockIterator // sudoku.dim, blockIterator % sudoku.dim)
            if len(region) > 3:
                tp = NakedTriplesAlgorithm.threePencils(region)
                for i in range(len(tp)):
                    for j in range(i + 1, len(tp)):
                        for k in range(j + 1, len(tp)):
                            pencils = tp[i].pencilMarks | tp[j].pencilMarks | tp[k].pencilMarks
                            if len(pencils) == 3:
                                for value in pencils:
                                    for tile in region:
                                        if tile.coors() != tp[i].coors() and \
                                                tile.coors() != tp[j].coors() and \
                                                tile.coors() != tp[k].coors() and value in tile.pencilMarks:
                                            command = DeletePencil(tile.column, tile.row, value)
                                            commandSet.add(command)

        return commandSet

    @staticmethod
    def threePencils(l):
        return [i for i in l if len(i.pencilMarks) <= 3]
