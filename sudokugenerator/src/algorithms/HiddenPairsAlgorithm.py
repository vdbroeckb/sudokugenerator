from ..commands.DeletePencil import DeletePencil


class HiddenPairsAlgorithm:
    difficulty = 7

    @staticmethod
    def execute(sudoku):
        commandSet = set()
        for colIterator in range(sudoku.dim ** 2):
            region = sudoku.getColumnEmptys(colIterator)
            if len(region) > 2:
                filled = sudoku.getColumnValuesSet(colIterator)
                for first in range(1, sudoku.dim ** 2):
                    if first not in filled:
                        fW = HiddenPairsAlgorithm.where(first, region)
                        if len(fW) == 2:
                            for second in range(first + 1, sudoku.dim ** 2 + 1):
                                if second not in filled:
                                    sW = HiddenPairsAlgorithm.where(second, region)
                                    if sW == fW:
                                        for coor in sW:
                                            for value in coor.pencilMarks:
                                                if value != first and value != second:
                                                    command = DeletePencil(coor.column, coor.row, value)
                                                    commandSet.add(command)

        for rowIterator in range(sudoku.dim ** 2):
            region = sudoku.getRowEmptys(rowIterator)
            if len(region) > 2:
                filled = sudoku.getRowValuesSet(rowIterator)
                for first in range(1, sudoku.dim ** 2):
                    if first not in filled:
                        fW = HiddenPairsAlgorithm.where(first, region)
                        if len(fW) == 2:
                            for second in range(first + 1, sudoku.dim ** 2 + 1):
                                if second not in filled:
                                    sW = HiddenPairsAlgorithm.where(second, region)
                                    if sW == fW:
                                        for coor in sW:
                                            for value in coor.pencilMarks:
                                                if value != first and value != second:
                                                    command = DeletePencil(coor.column, coor.row, value)
                                                    commandSet.add(command)

        for blockIterator in range(sudoku.dim ** 2):
            region = sudoku.getBlockEmptys(blockIterator // sudoku.dim, blockIterator % sudoku.dim)
            if len(region) > 2:
                filled = sudoku.getBlockValuesSet(blockIterator // sudoku.dim, blockIterator % sudoku.dim)
                for first in range(1, sudoku.dim ** 2):
                    if first not in filled:
                        fW = HiddenPairsAlgorithm.where(first, region)
                        if len(fW) == 2:
                            for second in range(first + 1, sudoku.dim ** 2 + 1):
                                if second not in filled:
                                    sW = HiddenPairsAlgorithm.where(second, region)
                                    if sW == fW:
                                        for coor in sW:
                                            for value in coor.pencilMarks:
                                                if value != first and value != second:
                                                    command = DeletePencil(coor.column, coor.row, value)
                                                    commandSet.add(command)

        return commandSet

    @staticmethod
    def where(value, s):
        return {i for i in s if value in i.pencilMarks}
