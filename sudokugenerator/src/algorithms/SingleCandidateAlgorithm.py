from ..commands.FillTile import FillTile
from ..commands.FillPencil import FillPencil


class SingleCandidateAlgorithm:
    difficulty = 2

    @staticmethod
    def inverseExecute(sudoku):
        commandSet = set()

        for col in range(sudoku.dim ** 2):
            for row in range(sudoku.dim ** 2):
                if not sudoku.getTile(col, row).isEmpty():
                    val = sudoku.getTile(col, row).value
                    command = FillPencil(col, row, val)

                    command.executeNoPencils(sudoku)
                    sudoku.generateTilePencilMarksRemove(col, row, val)

                    if SingleCandidateAlgorithm.isValidInverse(sudoku, col, row, val):
                        commandSet.add(command)

                    command.unExecute(sudoku)

        return commandSet

    @staticmethod
    def inverseExecuteConstrained(sudoku, col, row):
        commandSet = set()

        for columnIterator in range(sudoku.dim ** 2):
            if not sudoku.getTile(columnIterator, row).isEmpty():
                val = sudoku.getTile(columnIterator, row).value
                command = FillPencil(columnIterator, row, val)

                command.executeNoPencils(sudoku)
                sudoku.generateTilePencilMarksRemove(columnIterator, row, val)

                if SingleCandidateAlgorithm.isValidInverse(sudoku, columnIterator, row, val):
                    commandSet.add(command)

                command.unExecute(sudoku)

        for rowIterator in range(sudoku.dim ** 2):
            if not sudoku.getTile(col, rowIterator).isEmpty():
                val = sudoku.getTile(col, rowIterator).value
                command = FillPencil(col, rowIterator, val)

                command.executeNoPencils(sudoku)
                sudoku.generateTilePencilMarksRemove(col, rowIterator, val)

                if SingleCandidateAlgorithm.isValidInverse(sudoku, col, rowIterator, val):
                    commandSet.add(command)

                command.unExecute(sudoku)

        for columnIterator in range(col // sudoku.dim, (col // sudoku.dim + 1) * sudoku.dim):
            for rowIterator in range(row // sudoku.dim, (row // sudoku.dim + 1) * sudoku.dim):
                if not sudoku.getTile(columnIterator, rowIterator).isEmpty():
                    val = sudoku.getTile(columnIterator, rowIterator).value
                    command = FillPencil(columnIterator, rowIterator, val)

                    command.executeNoPencils(sudoku)
                    sudoku.generateTilePencilMarksRemove(columnIterator, rowIterator, val)

                    if SingleCandidateAlgorithm.isValidInverse(sudoku, columnIterator, rowIterator, val):
                        commandSet.add(command)

                    command.unExecute(sudoku)

        return commandSet

    @staticmethod
    def isValidInverse(sudoku, col, row, value):
        return len(sudoku.matrix[col][row].pencilMarks) == 1 and next(
            iter(sudoku.matrix[col][row].pencilMarks)) == value

    @staticmethod
    def execute(sudoku):
        commandSet = set()
        for columnIterator in range(sudoku.dim ** 2):
            for rowIterator in range(sudoku.dim ** 2):
                if len(sudoku.matrix[columnIterator][rowIterator].pencilMarks) == 1:
                    value = next(iter(sudoku.matrix[columnIterator][rowIterator].pencilMarks))
                    commandSet.add(FillTile(columnIterator, rowIterator, value))
                    return commandSet
        if len(commandSet):
            print("Gebruikt algortime 2, er zijn " + str(len(commandSet)) + " mogelijke commandos")
        return commandSet

    @staticmethod
    def requirements(sudoku, col, row, value):
        Req = AndRequirement()
        for v in range(sudoku.dim ** 2):
            if not v == value:
                Req.children.add(PencilMarkLacksRequirement(col, row, v))

        return Req
