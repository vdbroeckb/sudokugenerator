from ..commands.DeletePencil import DeletePencil


class MultipleLinesAlgorithm:
    difficulty = 4

    @staticmethod
    def execute(sudoku):
        sudoku.setTiles()
        commandSet = set()
        where = MultipleLinesAlgorithm.where
        dim = sudoku.dim
        blocks = [sudoku.getBlockAsSet(i // dim, i % dim) for i in range(dim ** 2)]
        for blockIter in range(dim):
            for colIter in range(dim):
                col = sudoku.getColumnSet(blockIter * 3 + colIter)
                colValues = sudoku.getSetPencils(col)
                for v in colValues:
                    wc = where(v, col)
                    if len(wc) <= dim:
                        for i in range(dim):
                            block = blocks[blockIter * 3 + i]
                            if wc <= block:
                                wb = where(v, block)
                                for coor in (wb - wc):
                                    commandSet.add(DeletePencil(coor.column, coor.row, v))

                col = sudoku.getRowSet(blockIter * 3 + colIter)
                colValues = sudoku.getSetPencils(col)
                for v in colValues:
                    wc = where(v, col)
                    if len(wc) <= dim:
                        for i in range(dim):
                            block = blocks[blockIter + 3 * i]
                            if wc <= block:
                                wb = where(v, block)
                                for coor in (wb - wc):
                                    commandSet.add(DeletePencil(coor.column, coor.row, v))

        return commandSet

    @staticmethod
    def where(value, s):
        return {i for i in s if value in i.pencilMarks}
