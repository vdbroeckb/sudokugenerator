from .NakedTriplesAlgorithm import *
from .HiddenPairsAlgorithm import *
from .HiddenTriplesAlgorithm import *
from .CandidateLinesAlgorithm import *
from .MultipleLinesAlgorithm import *


class NakedPairsAlgorithm:
    difficulty = 5

    @staticmethod
    def execute(sudoku):
        commandSet = set()
        for colIterator in range(sudoku.dim ** 2):
            region = sudoku.getColumnEmptys(colIterator)
            if len(region) > 2:
                tp = NakedPairsAlgorithm.twoPencils(region)
                for i in range(len(tp)):
                    for j in range(i + 1, len(tp)):
                        if tp[i].pencilMarks == tp[j].pencilMarks:
                            for value in tp[i].pencilMarks:
                                for tile in region:
                                    if tile.coors() != tp[i].coors() and \
                                            tile.coors() != tp[j].coors() and \
                                            value in tile.pencilMarks:
                                        command = DeletePencil(tile.column, tile.row, value)
                                        commandSet.add(command)

        for RowIterator in range(sudoku.dim ** 2):
            region = sudoku.getRowEmptys(RowIterator)
            if len(region) > 2:
                tp = NakedPairsAlgorithm.twoPencils(region)
                for i in range(len(tp)):
                    for j in range(i + 1, len(tp)):
                        if tp[i].pencilMarks == tp[j].pencilMarks:
                            for value in tp[i].pencilMarks:
                                for tile in region:
                                    if tile.coors() != tp[i].coors() and \
                                            tile.coors() != tp[j].coors() and \
                                            value in tile.pencilMarks:
                                        command = DeletePencil(tile.column, tile.row, value)
                                        commandSet.add(command)

        for blockIterator in range(sudoku.dim ** 2):
            region = sudoku.getBlockEmptys(blockIterator // sudoku.dim, blockIterator % sudoku.dim)
            if len(region) > 2:
                tp = NakedPairsAlgorithm.twoPencils(region)
                for i in range(len(tp)):
                    for j in range(i + 1, len(tp)):
                        if tp[i].pencilMarks == tp[j].pencilMarks:
                            for value in tp[i].pencilMarks:
                                for tile in region:
                                    if tile.coors() != tp[i].coors() and \
                                            tile.coors() != tp[j].coors() and \
                                            value in tile.pencilMarks:
                                        command = DeletePencil(tile.column, tile.row, value)
                                        commandSet.add(command)

        return commandSet

    @staticmethod
    def twoPencils(l):
        return [i for i in l if len(i.pencilMarks) == 2]
