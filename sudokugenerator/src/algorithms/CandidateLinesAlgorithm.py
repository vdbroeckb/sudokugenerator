from ..commands.FillPencil import FillPencil
from ..requirements.AndRequirement import *
from ..requirements.OrRequirement import *
from ..requirements.PencilMarkLacksRequirement import *
from ..algorithms.MultipleLinesAlgorithm import *
from ..commands.DeletePencil import DeletePencil


class CandidateLinesAlgorithm:
    difficulty = 3

    @staticmethod
    def execute(sudoku):
        sudoku.setTiles()
        commandSet = set()
        where = CandidateLinesAlgorithm.where
        dim = sudoku.dim
        for colBlock in range(dim):
            cols = [sudoku.getColumnSet(colBlock * 3 + i) for i in range(dim)]
            for rowBlock in range(dim):
                block = sudoku.getBlockAsSet(colBlock, rowBlock)
                blockValues = sudoku.getBlockPencils(colBlock, rowBlock)
                for v in blockValues:
                    wb = where(v, block)
                    if len(wb) <= dim:
                        for col in cols:
                            if wb <= col:
                                wc = where(v, col)
                                for coor in (wc - wb):
                                    commandSet.add(DeletePencil(coor.column, coor.row, v))
        for rowBlock in range(dim):
            cols = [sudoku.getRowSet(rowBlock * 3 + i) for i in range(dim)]
            for colBlock in range(sudoku.dim):
                block = sudoku.getBlockAsSet(colBlock, rowBlock)
                blockValues = sudoku.getBlockPencils(colBlock, rowBlock)
                for v in blockValues:
                    wb = where(v, block)
                    if len(wb) <= dim:
                        for col in cols:
                            if wb <= col:
                                wc = where(v, col)
                                for coor in (wc - wb):
                                    commandSet.add(DeletePencil(coor.column, coor.row, v))

        return commandSet

    @staticmethod
    def where(value, s):
        return {i for i in s if value in i.pencilMarks}

    @staticmethod
    def inverseExecute(sudoku):
        commandSet = set()

        for col in range(sudoku.dim ** 2):
            for row in range(sudoku.dim ** 2):
                if not sudoku.getTile(col, row).isEmpty():
                    val = sudoku.getTile(col, row).value
                    command = FillPencil(col, row, val)

                    command.executeNoPencils(sudoku)
                    sudoku.generateTilePencilMarksRemove(col, row, val)
                    sudoku.generateBlockPencilMarksRemove(col // sudoku.dim, row // sudoku.dim, val)
                    sudoku.generateColPencilMarksRemove(col, val)
                    sudoku.generateRowPencilMarksRemove(row, val)
                    if not CandidateLinesAlgorithm.isValidInverse(sudoku, col, row, val):
                        cs = CandidateLinesAlgorithm.execute(sudoku)
                        ms = MultipleLinesAlgorithm.execute(sudoku)
                        for i in cs:
                            i.execute(sudoku)
                        for i in ms:
                            i.execute(sudoku)

                        if CandidateLinesAlgorithm.isValidInverse(sudoku, col, row, val):
                            commandSet.add(command)

                        command.unExecute(sudoku)
                        sudoku.generatePencilMarks()
                    else:
                        command.unExecute(sudoku)

        return commandSet

    @staticmethod
    def isValidInverse(sudoku, col, row, value):
        for i in CandidateLinesAlgorithm.requirements(sudoku, col, row, value):
            if i.isSolved(sudoku):
                return True
        return False

    @staticmethod
    def requirements(sudoku, col, row, value):
        Req = AndRequirement()
        Req.children = [PencilMarkLacksRequirement(col, row, v + 1) for v in range(sudoku.dim ** 2) if v + 1 != value]

        retReq = OrRequirement()

        colReq = AndRequirement()
        colReq.children = [PencilMarkLacksRequirement(colIterator, row, value) for colIterator in range(sudoku.dim ** 2)
                           if colIterator != col]

        retReq.children.add(colReq)

        rowReq = AndRequirement()
        rowReq.children = [PencilMarkLacksRequirement(col, rowIterator, value) for rowIterator in range(sudoku.dim ** 2)
                           if rowIterator != row]

        retReq.children.add(rowReq)

        blockReq = AndRequirement()
        blockReq.children = [PencilMarkLacksRequirement(colIterator + (col // sudoku.dim) * sudoku.dim,
                                                        rowIterator + (row // sudoku.dim) * sudoku.dim,
                                                        value)
                             for colIterator in range(sudoku.dim)
                             for rowIterator in range(sudoku.dim)
                             if (not (colIterator == col % sudoku.dim)) or (not rowIterator == row % sudoku.dim)]

        retReq.children.add(blockReq)
        return [retReq, Req]
