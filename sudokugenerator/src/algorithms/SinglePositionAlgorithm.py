from ..commands.FillTile import FillTile
from ..commands.FillPencil import FillPencil
from ..requirements.AndRequirement import *
from ..requirements.OrRequirement import *
from ..requirements.PencilMarkLacksRequirement import *


class SinglePositionAlgorithm:
    difficulty = 1

    @staticmethod
    def inverseExecute(sudoku):
        commandSet = set()

        for columnIterator in range(sudoku.dim ** 2):
            for rowIterator in range(sudoku.dim ** 2):
                if not sudoku.getTile(columnIterator, rowIterator).isEmpty():
                    value = sudoku.getTile(columnIterator, rowIterator).value

                    command = FillPencil(columnIterator, rowIterator, value)
                    command.executeNoPencils(sudoku)

                    sudoku.generateBlockPencilMarksRemove(columnIterator // sudoku.dim,
                                                          rowIterator // sudoku.dim, value)
                    sudoku.generateColPencilMarksRemove(columnIterator, value)
                    sudoku.generateRowPencilMarksRemove(rowIterator, value)
                    if SinglePositionAlgorithm.isValidInverse(sudoku, columnIterator, rowIterator, value):
                        commandSet.add(command)

                    command.unExecute(sudoku)
        return commandSet

    @staticmethod
    def isValidInverse(sudoku, col, row, value):
        return SinglePositionAlgorithm.requirements(sudoku, col, row, value).isSolved(sudoku)

    @staticmethod
    def execute(sudoku):
        commandSet = set()

        for rowIterator in range(sudoku.dim ** 2):
            checkList = [0] * (sudoku.dim ** 2)
            for columnIterator in range(sudoku.dim ** 2):
                for pencilMark in sudoku.matrix[columnIterator][rowIterator].pencilMarks:
                    checkList[pencilMark-1] += 1
            for check in range(len(checkList)):
                if checkList[check] == 1:
                    for columnIterator in range(sudoku.dim ** 2):
                        if (check + 1) in sudoku.matrix[columnIterator][rowIterator].pencilMarks:
                            commandSet.add(FillTile(columnIterator, rowIterator, check+1))
        for columnIterator in range(sudoku.dim ** 2):
            checkList = [0] * (sudoku.dim ** 2)
            for rowIterator in range(sudoku.dim ** 2):
                for pencilMark in sudoku.matrix[columnIterator][rowIterator].pencilMarks:
                    checkList[pencilMark-1] += 1
            for check in range(len(checkList)):
                if checkList[check] == 1:
                    for rowIterator in range(sudoku.dim ** 2):
                        if (check + 1) in sudoku.matrix[columnIterator][rowIterator].pencilMarks:
                            commandSet.add(FillTile(columnIterator, rowIterator, check + 1))
        for col in range(sudoku.dim):
            for row in range(sudoku.dim):
                block = sudoku.matrixToList(sudoku.getBlock(col, row))
                checkList = [0] * (sudoku.dim ** 2)
                for i in block:
                    for pencilMark in i.pencilMarks:
                        checkList[pencilMark - 1] += 1
                for check in range(len(checkList)):
                    if checkList[check] == 1:
                        for i in range(sudoku.dim):
                            for j in range(sudoku.dim):
                                if (check + 1) in sudoku.matrix[col * sudoku.dim + i][row * sudoku.dim + j].pencilMarks:
                                    commandSet.add(FillTile(col * sudoku.dim + i, row * sudoku.dim + j, check + 1))
        return commandSet

    @staticmethod
    def requirements(sudoku, col, row, value):
        retReq = OrRequirement()
        colReq = AndRequirement()
        for colIterator in range(sudoku.dim ** 2):
            if not colIterator == col:
                colReq.children.add(PencilMarkLacksRequirement(colIterator, row, value))
        retReq.children.add(colReq)

        rowReq = AndRequirement()
        for rowIterator in range(sudoku.dim ** 2):
            if not rowIterator == row:
                rowReq.children.add(PencilMarkLacksRequirement(col, rowIterator, value))

        retReq.children.add(rowReq)

        blockReq = AndRequirement()
        for colIterator in range(sudoku.dim):
            for rowIterator in range(sudoku.dim):
                if (not (colIterator == col % sudoku.dim)) or (not rowIterator == row % sudoku.dim):
                    blockReq.children.add(PencilMarkLacksRequirement(colIterator + (col // sudoku.dim) * sudoku.dim,
                                                                     rowIterator + (row // sudoku.dim) * sudoku.dim,
                                                                     value))

        retReq.children.add(blockReq)
        return retReq
