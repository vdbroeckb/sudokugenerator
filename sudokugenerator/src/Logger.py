import sys
import time
import os


class Logger(object):

    def __init__(self):
        self.terminal = sys.stdout
        file = os.path.realpath(__file__)
        file = file[:-13]
        file += "log/"
        file += time.strftime("%y_%m_%d_%HH%MM%S.txt", time.localtime())
        print(file)
        self.log = open(file, "a")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        # this flush method is needed for python 3 compatibility.
        # this handles the flush command by doing nothing.
        # you might want to specify some extra behavior here.
        pass
