from src.RandomGenerator import *
from src.SudokuDrawWindowStub import *
from src.SudokuDrawWindow import *
from src.SudokuSolver import *
from src.SudokuParser import *
from src.Logger import *


def testDiffs():
    sys.stdout = Logger()  # comment to stop logging
    b = 0
    n = 10
    sudoku = Sudoku()
    sudoDraw = SudokuDrawWindowStub(600, True, True)

    sudoDraw.drawSudoku(sudoku)

    for i in range(n - b):
        sudoku = Sudoku()
        diff = "0" + str((b + i + 1))
        diff = diff[-2:]
        # print(diff)
        file = "sudokus/diffs/" + diff
        sudoku.parseFromFile(file, " ")
        sudoku.generatePencilMarks()
        sudoPl = sudoku.copy()

        t = time.time()
        diff = SudokuSolver.solveAndGetDifficulty(sudoku, 100, sudoDraw)
        pyTime = time.time() - t

        t = time.time()
        diffPl = SudokuSolver.solveAndGetDifficultyPl(sudoPl, 100, sudoDraw)
        plTime = time.time() - t

        print("Python diff = " + str(diff) + ", en duurde " + str(pyTime) + " seconden;       Prolog Diff = " + str(
            diffPl) + ", en duurde " + str(plTime) + " seconden")
        if diff == -1:
            sudoku.printText()
            print(sudoku.toPl())
            sudoDraw = SudokuDrawWindow(600, True, True)
            sudoDraw.clearHighlights()
            sudoDraw.clearNumbers()
            sudoDraw.clearPencilMarks()
            sudoDraw.drawSudoku(sudoku)
            # sudoDraw.pauseUntilClick()


def testEasyInverter():
    sys.stdout = Logger()   # comment to stop logging
    n = 10
    sudoku = Sudoku()
    sudoDraw = SudokuDrawWindowStub(600, True, True)
    # sudoDraw = SudokuDrawWindow(600, True, True)
    sudoDraw.drawSudoku(sudoku)
    maxtime = 60 * 30

    for i in range(n):
        start_time = time.time()
        # sudoku = RandomGenerator().generateRandomSudoku(9)
        sudoku = SudokuParser.parseFromFile("./sudokus/diffs/D03/0" + str(i))
        SudokuSolver.solveAndGetDifficulty(sudoku, sudoDraw)
        sudoku, iterations = SudokuSolver.invertSolveEasy(sudoku, maxtime, sudoDraw)
        if sudoku:
            diff = SudokuSolver.solveAndGetDifficulty(sudoku, sudoDraw)
            if diff == 2:
                s = "correct"
            else:
                s = "fout, moeilijkheid was : " + str(diff)
        else:
            diff = 0
            s = "foutief"

        print("{:5d}".format(i + 1) + ", time =, " + "{:13.7f}".format(time.time() - start_time) +
              ", seconds, " + s + ", Iterations =, " + "{:5d}".format(iterations), ", difficulty =, " + str(diff))

def testMediumInverter():
    sys.stdout = Logger()   # comment to stop logging
    n = 10
    sudoku = Sudoku()
    sudoDraw = SudokuDrawWindowStub(600, True, True)
    # sudoDraw = SudokuDrawWindow(600, True, True)
    sudoDraw.drawSudoku(sudoku)
    maxtime = 60 * 30

    for i in range(n):
        start_time = time.time()
        # sudoku = RandomGenerator().generateRandomSudoku(9)
        sudoku = SudokuParser.parseFromFile("./sudokus/diffs/D03/0" + str(i))
        SudokuSolver.solveAndGetDifficulty(sudoku, sudoDraw)
        sudoku, iterations = SudokuSolver.invertSolveMedium(sudoku, maxtime, sudoDraw)
        if sudoku:
            diff = SudokuSolver.solveAndGetDifficulty(sudoku, sudoDraw)
            if diff == 3 or diff == 4:
                s = "correct"
            else:
                s = "fout, moeilijkheid was : " + str(diff)
        else:
            diff = 0
            s = "foutief"

        print("{:5d}".format(i + 1) + ", time =, " + "{:13.7f}".format(time.time() - start_time) +
              ", seconds, " + s + ", Iterations =, " + "{:5d}".format(iterations), ", difficulty =, " + str(diff))


def testDifficultInverter():
    sys.stdout = Logger()  # comment to stop logging
    n = 10
    sudoku = Sudoku()
    sudoDraw = SudokuDrawWindowStub(600, True, True)
    sudoDraw.drawSudoku(sudoku)
    maxtime = 60 * 30

    for i in range(n):
        start_time = time.time()
        # sudoku = RandomGenerator().generateRandomSudoku(9)
        sudoku = SudokuParser.parseFromFile("./sudokus/diffs/D03/0" + str(i))
        SudokuSolver.solveAndGetDifficulty(sudoku, sudoDraw)
        sudoku, iterations = SudokuSolver.invertSolveDifficult(sudoku, maxtime, sudoDraw)
        if sudoku:
            diff = SudokuSolver.solveAndGetDifficulty(sudoku, sudoDraw)
            if 5 <= diff <= 8:
                s = "correct"
            else:
                s = "fout, moeilijkheid was : " + str(diff)
        else:
            diff = 0
            s = "foutief"

        print("{:5d}".format(i + 1) + ", time =, " + "{:13.7f}".format(time.time() - start_time) +
              ", seconds, " + s + ", Iterations =, " + "{:5d}".format(iterations), ", difficulty =, " + str(diff))
    # print(str(count) + " van de " + str(n) + " hadden een moeilijkheid van 5, 6, 7 of 8")
    # print(" dit duurde gemiddeld " + str((time.time() - total_time) / n) + " seconden")


def parsePrint(file):
    lfile = "./sudokus/" + file
    sudoku = SudokuParser.parseFromFile(lfile, " ")
    sudoku.generatePencilMarks()
    sudoku.print()
    print("\n")
    sudoku.printText()
    print("\n")


def outFile(sudoku):
    i = 0
    while True:
        file = str(i).zfill(3)
        if os.path.isfile('./sudokus/Gens/' + file):
            i += 1
        else:
            break
    f = open("./sudokus/Gens/" + file, "a")
    f.write(sudoku.toText())
    f.close()


def printTex():
    sys.stdout = Logger()
    n = 100
    sudoku = Sudoku()
    for f in range(n):
        file = str(f).zfill(3)
        sudoku.parseFromFile("./sudokus/Gens/" + file)
        sudoku.generatePencilMarks()
        retString = SudokuParser.toLatexBoekje(boekje)
        print(retString)


if __name__ == "__main__":
    testDifficultInverter()
    # testEasyInverter()
    # testMediumInverter()
    # parsePrint("diffs/10")
    # printTex()
