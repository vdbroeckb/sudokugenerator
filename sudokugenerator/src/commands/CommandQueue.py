from ..algorithms.CandidateLinesAlgorithm import *
from ..algorithms.MultipleLinesAlgorithm import *


class CommandQueue:
    commands = []
    commandsSet = frozenset()
    pencilCommands = set()
    toBreakRequirements = set()
    brokenRequirements = set()
    parent = None  # puur voor debug, ge  en functie
    iter = 0  # puur voor debug, geen functie

    def __init__(self, commands=None):
        if commands is None:
            commands = set()
        self.commands = commands
        self.commandsSet = frozenset(commands)
        self.toBreakRequirements = set()
        self.brokenRequirements = set()

    def execute(self, sudoku):
        for command in self.commands:
            command.execute(sudoku)

    def unExecute(self, sudoku):
        for command in reversed(self.commands):
            command.unExecute(sudoku)

    def addCommand(self, command):
        self.commands.append(command)

    # HEURESTIEK, nog af te stellen
    def priority(self):
        return len(self.toBreakRequirements) ** 1

    def newEasyQue(self, sudoku, command, requirements):
        ret = CommandQueue(self.commands + [command])
        ret.toBreakRequirements = self.toBreakRequirements.copy()

        ret.toBreakRequirements |= set(requirements)
        for requirement in ret.toBreakRequirements:
            if not requirement.isSolved(sudoku):
                ret.brokenRequirements.add(requirement)
        ret.toBreakRequirements -= ret.brokenRequirements
        ret.parent = self
        return ret

    def newMediumQue(self, sudoku, command, requirements):
        ret = CommandQueue(self.commands + [command])
        ret.toBreakRequirements = self.toBreakRequirements.copy()

        ret.toBreakRequirements |= set(requirements)
        sudoku.generatePencilMarks()
        for requirement in ret.toBreakRequirements:
            if not requirement.isSolved(sudoku):
                ret.brokenRequirements.add(requirement)

        ret.toBreakRequirements -= ret.brokenRequirements

        command.unExecute(sudoku)
        sudoku.generatePencilMarks()
        ret.parent = self
        return ret

    def newDifficultQue(self, sudoku, command, requirements):
        ret = CommandQueue(self.commands + [command])
        ret.toBreakRequirements = self.toBreakRequirements.copy()

        ret.toBreakRequirements |= set(requirements)
        sudoku.generatePencilMarks()
        s = CandidateLinesAlgorithm.execute(sudoku) | MultipleLinesAlgorithm.execute(sudoku)
        while len(s):
            for i in s:
                i.execute(sudoku)
            s = CandidateLinesAlgorithm.execute(sudoku) | MultipleLinesAlgorithm.execute(sudoku)
        for requirement in ret.toBreakRequirements:
            if not requirement.isSolved(sudoku):
                ret.brokenRequirements.add(requirement)

        ret.toBreakRequirements -= ret.brokenRequirements

        command.unExecute(sudoku)
        sudoku.generatePencilMarks()
        ret.parent = self
        return ret

    def __lt__(self, other):
        return self.priority() <= other.priority()

    def __eq__(self, other):
        return self.commandsSet == other.commandsSet

    def __hash__(self):
        return hash(self.commandsSet)
