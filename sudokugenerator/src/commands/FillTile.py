from ..Tile import *


class FillTile:
    row = 0
    column = 0
    value = 0

    def __init__(self, column, row, value):
        self.row = row
        self.column = column
        self.value = value

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, FillTile):
            return self.row == other.row and self.column == other.column and self.value == other.value
        return False

    def __hash__(self):
        return hash((self.row, self.column, self.value))

    def execute(self, sudoku):
        sudoku.matrix[self.column][self.row] = Tile(self.value, {}, self.column, self.row)
        sudoku.generateFastPencilMarkAdd(self.column, self.row, self.value)

    def print(self):
        print("Vul een " + str(self.value) + " in op rij " + str(self.row) + " en kolom " + str(self.column))

    def draw(self, sudoDraw):
        sudoDraw.highlightTile(self.column, self.row)
