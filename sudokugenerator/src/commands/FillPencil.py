from ..Tile import *


class FillPencil:
    row = 0
    column = 0
    value = ()
    oldValue = 0

    def __init__(self, column, row, value):
        self.row = row
        self.column = column
        self.value = (value,)
        self.oldValue = value

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, FillPencil):
            return self.row == other.row and self.column == other.column
        return False

    def __hash__(self):
        return hash((self.row, self.column))

    def execute(self, sudoku):
        sudoku.setTile(self.column, self.row, Tile(0, set(self.value), self.column, self.row))
        sudoku.generateFastPencilMarkRemove(self.column, self.row, self.oldValue)

    def executeNoPencils(self, sudoku):
        sudoku.setTile(self.column, self.row, Tile(0, set(self.value), self.column, self.row))

    def unExecute(self, sudoku):
        sudoku.setTile(self.column, self.row, Tile(self.oldValue, set(), self.column, self.row))
        sudoku.generateFastPencilMarkAdd(self.column, self.row, self.oldValue)

    def print(self):
        print("Vul een " + str(self.value) + " in op rij " + str(self.row) + " en kolom " + str(self.column))

    def draw(self, sudoDraw):
        sudoDraw.highlightTile(self.column, self.row)
