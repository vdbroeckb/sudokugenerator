class DeletePencil:
    row = 0
    column = 0
    value = 0

    def __init__(self, column, row, value):
        self.row = row
        self.column = column
        self.value = value

    def execute(self, sudoku):
        sudoku.matrix[self.column][self.row].deletePencil(self.value)

    def unExecute(self, sudoku):
        sudoku.matrix[self.column][self.row].addPencil(self.value)

    def print(self):
        print("Verwijder de " + str(self.value) + " op rij " + str(self.row) + " en kolom " + str(self.column))

    def draw(self, sudoDraw):
        sudoDraw.highlightTile(self.column, self.row)
