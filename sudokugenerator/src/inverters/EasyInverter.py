from ..algorithms.CandidateLinesAlgorithm import *


class EasyInverter:
    @staticmethod
    def inverseExecute(sudoku, prevQue):
        ques = []
        last = prevQue.commands[-1]
        for col in range(sudoku.dim ** 2):
            for row in range(sudoku.dim ** 2):
                if not sudoku.getTile(col, row).isEmpty() and (
                        EasyInverter.commandsInfluence(sudoku, last, col, row) or
                        len(prevQue.toBreakRequirements) <= 2):
                    val = sudoku.getTile(col, row).value
                    command = FillPencil(col, row, val)
                    command.execute(sudoku)
                    reqs = EasyInverter.requirements(sudoku, col, row, val)
                    if EasyInverter.isValidInverse(sudoku, col, row, val):
                        ques.append(prevQue.newEasyQue(sudoku, command, reqs))
                    command.unExecute(sudoku)
        return ques

    @staticmethod
    def isValidInverse(sudoku, col, row, value):
        for i in EasyInverter.requirements(sudoku, col, row, value):
            if i.isSolved(sudoku):
                return True
        if EasyInverter.cellRequirements(sudoku, col, row, value).isSolved(sudoku):
            return True
        return False

    @staticmethod
    def requirements(sudoku, col, row, value):
        colReq = AndRequirement()
        colReq.children = [PencilMarkLacksRequirement(colIterator, row, value) for colIterator in range(sudoku.dim ** 2)
                           if colIterator != col]

        rowReq = AndRequirement()
        rowReq.children = [PencilMarkLacksRequirement(col, rowIterator, value) for rowIterator in range(sudoku.dim ** 2)
                           if rowIterator != row]

        blockReq = AndRequirement()
        blockReq.children = [PencilMarkLacksRequirement(colIterator + (col // sudoku.dim) * sudoku.dim,
                                                        rowIterator + (row // sudoku.dim) * sudoku.dim,
                                                        value)
                             for colIterator in range(sudoku.dim)
                             for rowIterator in range(sudoku.dim)
                             if (not (colIterator == col % sudoku.dim)) or (not rowIterator == row % sudoku.dim)]
        return [rowReq, colReq, blockReq]

    @staticmethod
    def cellRequirements(sudoku, col, row, value):
        Req = AndRequirement()
        Req.children = [PencilMarkLacksRequirement(col, row, v + 1) for v in range(sudoku.dim ** 2) if v + 1 != value]
        return Req

    @staticmethod
    def commandsInfluence(sudoku, command, col, row):
        return command.column == col \
               or command.row == row \
               or (command.row // sudoku.dim == row // sudoku.dim
                   and command.column // sudoku.dim == col // sudoku.dim)
