from ..algorithms.NakedPairsAlgorithm import *
from ..algorithms.NakedTriplesAlgorithm import *
from ..algorithms.HiddenPairsAlgorithm import *
from ..algorithms.HiddenTriplesAlgorithm import *
from ..algorithms.CandidateLinesAlgorithm import *
from ..algorithms.MultipleLinesAlgorithm import *


class MediumInverter:

    @staticmethod
    def inverseExecute(sudoku, prevQue):
        ques = []
        last = prevQue.commands[-1]
        for col in range(sudoku.dim ** 2):
            for row in range(sudoku.dim ** 2):
                if not sudoku.getTile(col, row).isEmpty() and (
                        MediumInverter.commandsInfluence(sudoku, last, col, row) or
                        len(prevQue.toBreakRequirements) <= 2):
                    val = sudoku.getTile(col, row).value
                    command = FillPencil(col, row, val)
                    command.execute(sudoku)
                    reqs = MediumInverter.requirements(sudoku, col, row, val)
                    if MediumInverter.isValidInverse(sudoku, col, row, val):
                        ques.append(prevQue.newMediumQue(sudoku, command, reqs))
                    else:
                        s = CandidateLinesAlgorithm.execute(sudoku) | MultipleLinesAlgorithm.execute(sudoku)
                        while len(s):
                            for i in s:
                                i.execute(sudoku)
                            s = CandidateLinesAlgorithm.execute(sudoku) | MultipleLinesAlgorithm.execute(sudoku)
                        if MediumInverter.isValidInverse(sudoku, col, row, val):
                            ques.append(prevQue.newMediumQue(sudoku, command, reqs))

                    command.unExecute(sudoku)
                    sudoku.generatePencilMarks()

        return ques

    @staticmethod
    def isValidInverse(sudoku, col, row, value):
        for i in MediumInverter.requirements(sudoku, col, row, value):
            if i.isSolved(sudoku):
                return True
        return False

    @staticmethod
    def requirements(sudoku, col, row, value):
        Req = AndRequirement()
        Req.children = [PencilMarkLacksRequirement(col, row, v + 1) for v in range(sudoku.dim ** 2) if v + 1 != value]

        colReq = AndRequirement()
        colReq.children = [PencilMarkLacksRequirement(colIterator, row, value) for colIterator in range(sudoku.dim ** 2)
                           if colIterator != col]

        rowReq = AndRequirement()
        rowReq.children = [PencilMarkLacksRequirement(col, rowIterator, value) for rowIterator in range(sudoku.dim ** 2)
                           if rowIterator != row]

        blockReq = AndRequirement()
        blockReq.children = [PencilMarkLacksRequirement(colIterator + (col // sudoku.dim) * sudoku.dim,
                                                        rowIterator + (row // sudoku.dim) * sudoku.dim,
                                                        value)
                             for colIterator in range(sudoku.dim)
                             for rowIterator in range(sudoku.dim)
                             if (not (colIterator == col % sudoku.dim)) or (not rowIterator == row % sudoku.dim)]

        return [Req, rowReq, colReq, blockReq]

    @staticmethod
    def commandsInfluence(sudoku, command, col, row):
        return command.column == col \
               or command.row == row \
               or (command.row // sudoku.dim == row // sudoku.dim
                   and command.column // sudoku.dim == col // sudoku.dim)
