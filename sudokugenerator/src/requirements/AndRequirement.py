class AndRequirement:
    children = set()

    def __init__(self, children=None):
        if children is None:
            children = set()
        self.children = self.children.union(children)

    def isSolved(self, sudoku):
        for i in self.children:
            if not i.isSolved(sudoku):
                return False
        return True
