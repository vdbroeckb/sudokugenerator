class OrRequirement:
    children = set()

    def __init__(self, children=None):
        if children is None:
            children = set()
        self.children = self.children.union(children)

    def isSolved(self, sudoku):
        for i in self.children:
            if i.isSolved(sudoku):
                return True
        return False
