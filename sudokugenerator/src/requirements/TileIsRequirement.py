class TileIsRequirement:
    row = 0
    col = 0
    value = 0

    def __init__(self, col, row, value):
        self.row = row
        self.col = col
        self.value = value

    def isSolved(self, sudoku):
        return sudoku.getTile(self.col, self.row).value == self.value
