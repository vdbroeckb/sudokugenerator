from graphics import *
import math


class SudokuDrawWindow:

    def __init__(self, size=600, drawsEmpty=True, drawsPencilMark=False):
        self.win = GraphWin("", size, size)
        self.drawsEmpty = drawsEmpty
        self.drawsPencilMark = drawsPencilMark
        self.selectedBox = Rectangle(Point(0, 0), Point(0, 0))
        self.selectedBox.setOutline("green")
        self.selectedBox.setWidth(4)
        self.sudokuDim = 9
        self.highlights = []
        self.pencilMarks = []
        self.numbers = []
        self.grid = []

    def clearNumbers(self):
        for numb in self.numbers:
            numb.undraw()
        self.numbers = []

    def clearPencilMarks(self):
        for mark in self.pencilMarks:
            mark.undraw()
        self.pencilMarks = []

    def matrixToScreen(self, col, row):
        return Point(col * self.win.height / self.sudokuDim, row * self.win.height / self.sudokuDim)

    def highlightTile(self, col, row, color="red", width=4):
        h = Rectangle(self.matrixToScreen(col, row), self.matrixToScreen(col+1, row+1))
        h.setOutline(color)
        h.setWidth(width)
        self.highlights.append(h)
        h.draw(self.win)

    def clearHighlights(self):
        for h in self.highlights:
            h.undraw()

    def selectRow(self, row):
        self.selectedBox.undraw()
        self.selectedBox.p1 = self.matrixToScreen(0, row)
        self.selectedBox.p2 = self.matrixToScreen(9, row + 1)
        self.selectedBox.draw(self.win)

    def selectColumn(self, col):
        self.selectedBox.undraw()
        self.selectedBox.p1 = self.matrixToScreen(col, 0)
        self.selectedBox.p2 = self.matrixToScreen(col + 1, 9)
        self.selectedBox.draw(self.win)

    def selectTile(self, col, row):
        self.selectedBox.undraw()
        self.selectedBox.p1 = self.matrixToScreen(col, row)
        self.selectedBox.p2 = self.matrixToScreen(col + 1, row + 1)
        self.selectedBox.draw(self.win)

    def selectBox(self, col, row):
        self.selectedBox.undraw()
        self.selectedBox.p1 = self.matrixToScreen(col * 3, row * 3)
        self.selectedBox.p2 = self.matrixToScreen((col + 1) * 3, (row + 1) * 3)
        self.selectedBox.draw(self.win)

    def drawSudoku(self, sudoku):
        self.sudokuDim = sudoku.dim ** 2
        if not len(self.grid):
            self.drawGrid()
        self.drawNumbers(sudoku)

    def drawNumbers(self, sudoku):
        for col in range(sudoku.dim * sudoku.dim):
            for row in range(sudoku.dim * sudoku.dim):
                if sudoku.getTile(col, row).isEmpty() and not self.drawsEmpty:
                    continue

                if sudoku.getTile(col, row).isEmpty() and self.drawsPencilMark:
                    self.drawPencilMark(col, row, sudoku.getTile(col, row).getPencilMarks())
                else:
                    t = Text(self.matrixToScreen(col + 0.5, row + 0.5), sudoku.getTile(col, row).value)
                    t.draw(self.win)
                    self.numbers.append(t)

    def drawGrid(self):
        for col in range(self.sudokuDim + 1):
            line = Line(self.matrixToScreen(0, col), self.matrixToScreen(self.sudokuDim, col))
            if col % math.sqrt(self.sudokuDim) == 0:
                line.setWidth(4)
            line.draw(self.win)
            self.grid.append(line)
        for row in range(self.sudokuDim + 1):
            line = Line(self.matrixToScreen(row, 0), self.matrixToScreen(row, self.sudokuDim))
            if row % math.sqrt(self.sudokuDim) == 0:
                line.setWidth(4)
            line.draw(self.win)
            self.grid.append(line)

    def drawPencilMark(self, col, row, pencilMark):
        for val in pencilMark:
            if val <= 9:
                point = self.matrixToScreen(col + ((val - 1) % math.sqrt(self.sudokuDim) + 1) /
                                            (math.sqrt(self.sudokuDim) + 1),
                                            row + (int((val - 1) / math.sqrt(self.sudokuDim)) + 1) /
                                            (math.sqrt(self.sudokuDim) + 1))
                t = Text(point, str(val))
                t.setOutline("gray")
                t.draw(self.win)
                self.pencilMarks.append(t)

    def clearAndDraw(self, sudoku):
        self.clearHighlights()
        self.clearNumbers()
        self.clearPencilMarks()
        self.drawSudoku(sudoku)

    def pauseUntilClick(self):
        self.win.getMouse()

    def close(self):
        self.win.close()
